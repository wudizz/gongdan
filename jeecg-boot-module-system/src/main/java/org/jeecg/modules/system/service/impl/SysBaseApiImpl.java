package org.jeecg.modules.system.service.impl;

import java.io.*;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.dingtalk.api.response.OapiCrmAuthGroupMemberListResponse;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.util.internal.StringUtil;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecg.common.constant.CacheConstant;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.DataBaseConstant;
import org.jeecg.common.constant.WebsocketConst;
import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.vo.*;
import org.jeecg.common.util.*;
import org.jeecg.common.util.oss.OssBootUtil;
import org.jeecg.modules.message.entity.SysMessageTemplate;
import org.jeecg.modules.message.service.ISysMessageTemplateService;
import org.jeecg.modules.message.websocket.WebSocket;
import org.jeecg.modules.system.aspect.DictAspect;
import org.jeecg.modules.system.entity.*;
import org.jeecg.modules.system.mapper.*;
import org.jeecg.modules.system.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Description: 底层共通业务API，提供其他独立模块调用
 * @Author: scott
 * @Date:2019-4-20 
 * @Version:V1.0
 */
@Slf4j
@Service
public class SysBaseApiImpl implements ISysBaseAPI {
	@Autowired
	private ISysDictService dictService;
	@Autowired
	DictAspect DictAspect;
	/** 当前系统数据库类型 */
	private static String DB_TYPE = "";
	@Autowired
	private ISysMessageTemplateService sysMessageTemplateService;
	@Resource
	private SysLogMapper sysLogMapper;
	@Autowired
	private SysUserMapper userMapper;
	@Autowired
	private SysUserRoleMapper sysUserRoleMapper;
	@Autowired
	private ISysDepartService sysDepartService;
	@Autowired
	private ISysDictService sysDictService;
	@Resource
	private SysAnnouncementMapper sysAnnouncementMapper;
	@Resource
	private SysAnnouncementSendMapper sysAnnouncementSendMapper;
	@Resource
    private WebSocket webSocket;
	@Resource
	private SysRoleMapper roleMapper;
	@Resource
	private SysDepartMapper departMapper;
	@Resource
	private SysCategoryMapper categoryMapper;

	@Autowired
	private ISysDataSourceService dataSourceService;
	@Autowired
	private ISysUserService sysUserService;
	@Autowired
	private ISysRoleService sysRoleService;
	@Autowired
	private SysUserMapper sysUserMapper;

	@Override
	public void addLog(String LogContent, Integer logType, Integer operatetype) {
		SysLog sysLog = new SysLog();
		//注解上的描述,操作日志内容
		sysLog.setLogContent(LogContent);
		sysLog.setLogType(logType);
		sysLog.setOperateType(operatetype);

		//请求的方法名
		//请求的参数

		try {
			//获取request
			HttpServletRequest request = SpringContextUtils.getHttpServletRequest();
			//设置IP地址
			sysLog.setIp(IPUtils.getIpAddr(request));
		} catch (Exception e) {
			sysLog.setIp("127.0.0.1");
		}

		//获取登录用户信息
		LoginUser sysUser = (LoginUser)SecurityUtils.getSubject().getPrincipal();
		if(sysUser!=null){
			sysLog.setUserid(sysUser.getUsername());
			sysLog.setUsername(sysUser.getRealname());

		}
		sysLog.setCreateTime(new Date());
		//保存系统日志
		sysLogMapper.insert(sysLog);
	}

	@Override
	@Cacheable(cacheNames=CacheConstant.SYS_USERS_CACHE, key="#username")
	public LoginUser getUserByName(String username) {
		if(oConvertUtils.isEmpty(username)) {
			return null;
		}
		LoginUser loginUser = new LoginUser();
		SysUser sysUser = userMapper.getUserByName(username);
		if(sysUser==null) {
			return null;
		}
		BeanUtils.copyProperties(sysUser, loginUser);
		return loginUser;
	}
	
	@Override
	public LoginUser getUserById(String id) {
		if(oConvertUtils.isEmpty(id)) {
			return null;
		}
		LoginUser loginUser = new LoginUser();
		SysUser sysUser = userMapper.selectById(id);
			if(sysUser==null) {
				return null;
		}
		BeanUtils.copyProperties(sysUser, loginUser);
		return loginUser;
	}
	@Override
	public boolean setUserRole(String userName,String roleCode){

		List<String> roles=sysUserService.getRole(userName);
		String hasMark="0";
		if(null!=roles&&roles.size()>0){
			for(String r:roles){
				if(r.equals(roleCode)){
					hasMark="1";
					break;
				}
			}
		}
		if("0".equals(hasMark)){
			SysUser user=null;
			user=userMapper.getUserByName(userName);
			if(null!=user){
				QueryWrapper<SysRole> queryWrapper=new QueryWrapper<SysRole>();
				queryWrapper.eq("role_code",roleCode);
				List<SysRole> pageList = sysRoleService.list(queryWrapper);
				if(null!=pageList&&pageList.size()>0){
					sysUserService.addUserWithRole(user,pageList.get(0).getId());
				}else{
					log.error("角色"+roleCode+"不存在");
				}
			}else{
				log.error("用户"+userName+"不存在");
			}


		}

		return true;
	}
	@Override
	public List<String> getRolesByUsername(String username) {
		return sysUserRoleMapper.getRoleByUserName(username);
	}

	@Override
	public List<String> getDepartIdsByUsername(String username) {
		List<SysDepart> list = sysDepartService.queryDepartsByUsername(username);
		List<String> result = new ArrayList<>(list.size());
		for (SysDepart depart : list) {
			result.add(depart.getId());
		}
		return result;
	}

	@Override
	public List<String> getDepartNamesByUsername(String username) {
		List<SysDepart> list = sysDepartService.queryDepartsByUsername(username);
		List<String> result = new ArrayList<>(list.size());
		for (SysDepart depart : list) {
			result.add(depart.getDepartName());
		}
		return result;
	}

	@Override
	public String getDatabaseType() throws SQLException {
		if(oConvertUtils.isNotEmpty(DB_TYPE)){
			return DB_TYPE;
		}
		DataSource dataSource = SpringContextUtils.getApplicationContext().getBean(DataSource.class);
		return getDatabaseTypeByDataSource(dataSource);
	}

	@Override
	@Cacheable(value = CacheConstant.SYS_DICT_CACHE,key = "#code")
	public List<DictModel> queryDictItemsByCode(String code) {
		return sysDictService.queryDictItemsByCode(code);
	}

	@Override
	public List<DictModel> queryTableDictItemsByCode(String table, String text, String code) {
		return sysDictService.queryTableDictItemsByCode(table, text, code);
	}

	@Override
	public List<DictModel> queryAllDepartBackDictModel() {
		return sysDictService.queryAllDepartBackDictModel();
	}

    @Override
    public List<JSONObject> queryAllDepart(Wrapper wrapper) {
        //noinspection unchecked
        return JSON.parseArray(JSON.toJSONString(sysDepartService.list(wrapper))).toJavaList(JSONObject.class);
    }

    @Override
	public void sendSysAnnouncement(String fromUser, String toUser, String title, String msgContent) {
		this.sendSysAnnouncement(fromUser, toUser, title, msgContent, CommonConstant.MSG_CATEGORY_2);
	}

	@Override
	public void sendSysAnnouncement(String fromUser, String toUser, String title, String msgContent, String setMsgCategory) {
		SysAnnouncement announcement = new SysAnnouncement();
		announcement.setTitile(title);
		announcement.setMsgContent(msgContent);
		announcement.setSender(fromUser);
		announcement.setPriority(CommonConstant.PRIORITY_M);
		announcement.setMsgType(CommonConstant.MSG_TYPE_UESR);
		announcement.setSendStatus(CommonConstant.HAS_SEND);
		announcement.setUserIds(toUser);
		announcement.setSendTime(new Date());
		announcement.setMsgCategory(setMsgCategory);
		announcement.setDelFlag(String.valueOf(CommonConstant.DEL_FLAG_0));
		sysAnnouncementMapper.insert(announcement);
		// 2.插入用户通告阅读标记表记录
		String userId = toUser;
		String[] userIds = userId.split(",");
		String anntId = announcement.getId();
		for(int i=0;i<userIds.length;i++) {
			if(oConvertUtils.isNotEmpty(userIds[i])) {
				SysUser sysUser = userMapper.getUserByName(userIds[i]);
				if(sysUser==null) {
					continue;
				}
				SysAnnouncementSend announcementSend = new SysAnnouncementSend();
				announcementSend.setAnntId(anntId);
				announcementSend.setUserId(sysUser.getId());
				announcementSend.setReadFlag(CommonConstant.NO_READ_FLAG);
				sysAnnouncementSendMapper.insert(announcementSend);
				JSONObject obj = new JSONObject();
		    	obj.put(WebsocketConst.MSG_CMD, WebsocketConst.CMD_USER);
		    	obj.put(WebsocketConst.MSG_USER_ID, sysUser.getId());
				obj.put(WebsocketConst.MSG_ID, announcement.getId());
				obj.put(WebsocketConst.MSG_TXT, announcement.getTitile());
		    	webSocket.sendOneMessage(sysUser.getId(), obj.toJSONString());
			}
		}

	}

	@Override
	public void sendSysAnnouncement(String fromUser, String toUser, String title, String msgContent, String setMsgCategory, String busType, String busId) {
		SysAnnouncement announcement = new SysAnnouncement();
		announcement.setTitile(title);
		announcement.setMsgContent(msgContent);
		announcement.setSender(fromUser);
		announcement.setPriority(CommonConstant.PRIORITY_M);
		announcement.setMsgType(CommonConstant.MSG_TYPE_UESR);
		announcement.setSendStatus(CommonConstant.HAS_SEND);
		announcement.setSendTime(new Date());
		announcement.setMsgCategory(setMsgCategory);
		announcement.setDelFlag(String.valueOf(CommonConstant.DEL_FLAG_0));
		announcement.setBusId(busId);
		announcement.setBusType(busType);
		announcement.setOpenType(SysAnnmentTypeEnum.getByType(busType).getOpenType());
		announcement.setOpenPage(Objects.requireNonNull(SysAnnmentTypeEnum.getByType(busType)).getOpenPage());
		sysAnnouncementMapper.insert(announcement);
		// 2.插入用户通告阅读标记表记录
		String userId = toUser;
		String[] userIds = userId.split(",");
		String anntId = announcement.getId();
		for(int i=0;i<userIds.length;i++) {
			if(oConvertUtils.isNotEmpty(userIds[i])) {
				SysUser sysUser = userMapper.getUserByName(userIds[i]);
				if(sysUser==null) {
					continue;
				}
				SysAnnouncementSend announcementSend = new SysAnnouncementSend();
				announcementSend.setAnntId(anntId);
				announcementSend.setUserId(sysUser.getId());
				announcementSend.setReadFlag(CommonConstant.NO_READ_FLAG);
				sysAnnouncementSendMapper.insert(announcementSend);
				JSONObject obj = new JSONObject();
				obj.put(WebsocketConst.MSG_CMD, WebsocketConst.CMD_USER);
				obj.put(WebsocketConst.MSG_USER_ID, sysUser.getId());
				obj.put(WebsocketConst.MSG_ID, announcement.getId());
				obj.put(WebsocketConst.MSG_TXT, announcement.getTitile());
				webSocket.sendOneMessage(sysUser.getId(), obj.toJSONString());
			}
		}
	}
	public boolean changeRegion(String userName,String region){
		if(StringUtil.isNullOrEmpty(userName)||StringUtil.isNullOrEmpty(region)){
			return false;
		}
		SysUser sysUser=sysUserService.getUserByName(userName);
		if(null==sysUser){
			return false;
		}
		sysUser.setRegion(region);
		return sysUserService.changeRegion(sysUser);
	}

	//获取所有用户
	public JSONArray getAllUser(){
		List<SysUser> rel=new ArrayList<SysUser>();
		JSONArray relObj=null;
		QueryWrapper<SysUser> sysUserQueryWrapper=new QueryWrapper<SysUser>();
		sysUserQueryWrapper.select("id,username,del_flag");
		rel=sysUserService.list(sysUserQueryWrapper);
		if(null!=rel&&rel.size()>0){
			try {
				relObj = JSONArray.parseArray(JSONArray.toJSONString(rel));
			}catch (Exception e){
				log.error(e.getMessage(),e);
			}
		}
		return relObj;
	}
	//获取所有用户
	public List<JSONObject> getAllUserNames(){
		List<SysUser> rel=new ArrayList<SysUser>();
		List<JSONObject> jsonObjects=new ArrayList<JSONObject>();
		JSONArray relObj=null;
		QueryWrapper<SysUser> sysUserQueryWrapper=new QueryWrapper<SysUser>();
		sysUserQueryWrapper.select("id,realname,username,del_flag");
		rel=sysUserService.list(sysUserQueryWrapper);
		if(null!=rel&&rel.size()>0){
			try {
//				jsonObjects=this.parseDictTextList(rel);
				for(SysUser sysUser:rel){
					JSONObject jsonObject=new JSONObject();
					jsonObject.put("value",sysUser.getId());
					jsonObject.put("userName",sysUser.getUsername());
					jsonObject.put("label",sysUser.getRealname());
					jsonObjects.add(jsonObject);
				}
//				relObj = JSONArray.parseArray(JSONArray.toJSONString(rel));
			}catch (Exception e){
				log.error(e.getMessage(),e);
			}
		}
		return jsonObjects;
	}
	//软删除
	public boolean softDelUser(String userId){
		if(StringUtil.isNullOrEmpty(userId)||StringUtil.isNullOrEmpty(userId)){
			return false;
		}
//		sysUser.setDelFlag(1);
		return sysUserService.removeById(userId);
	}
	//新增用户
	public boolean addUser(String userName,String avatar,String realName,String phone){
		SysUser su=sysUserService.getUserByName(userName);
		if(null!=su){
			return false;
		}
		QueryWrapper<SysUser> sysUserQueryWrapper=new QueryWrapper<SysUser>();
		sysUserQueryWrapper.eq("username",userName);
		/**
		 * 查询被逻辑删除的用户
		 */
		List<SysUser> users=sysUserMapper.selectLogicDeleted(sysUserQueryWrapper);
		if(null!=users&& users.size()>0){
			SysUser updateUser = new SysUser();
			updateUser.setUpdateBy("无形的手");
			updateUser.setUpdateTime(new Date());
			int rel=sysUserMapper.revertLogicDeleted(users.get(0).getId(),updateUser);
			if(rel>0){
				return true;
			}else{
				return false;
			}
		}
		SysUser sysUserExcel = new SysUser();
		if (org.apache.commons.lang.StringUtils.isBlank(sysUserExcel.getPassword())) {
			// 密码默认为 “Pass_666888”
			sysUserExcel.setPassword("Pass_666888");
		}
		// 密码加密加盐
		String salt = oConvertUtils.randomGen(8);
		sysUserExcel.setSalt(salt);
		sysUserExcel.setUsername(userName);
		sysUserExcel.setRealname(realName);
		String passwordEncode = PasswordUtil.encrypt(sysUserExcel.getUsername(), sysUserExcel.getPassword(), salt);
		sysUserExcel.setPassword(passwordEncode);
		sysUserExcel.setDelFlag(0);
		sysUserExcel.setAvatar(avatar);
		sysUserExcel.setDingId(userName);
		sysUserExcel.setPhone(phone);
		sysUserExcel.setStatus(1);

		try {
			sysUserService.save(sysUserExcel);
			QueryWrapper<SysRole> queryWrapper=new QueryWrapper<SysRole>();
			queryWrapper.eq("role_code","common");
			List<SysRole> pageList = sysRoleService.list(queryWrapper);
			String roleCode="";
			if(null!=pageList&&pageList.size()>0){
				roleCode=pageList.get(0).getId();
				sysUserService.addUserWithRole(sysUserExcel, roleCode);
				LambdaQueryWrapper<SysDepart> queryDep = new LambdaQueryWrapper<SysDepart>();
				queryDep.eq(SysDepart::getParentId, "").or().isNull(SysDepart::getParentId);
				queryDep.orderByDesc(SysDepart::getOrgCode);
				List<SysDepart> departList = sysDepartService.list(queryDep);
				String selectedDeparts="";
				if(null!=departList&&departList.size()>0){
					selectedDeparts=departList.get(0).getId();
				}
				sysUserService.addUserWithDepart(sysUserExcel, selectedDeparts);
			}else{
				log.info("common角色不存");
			}

		} catch (Exception e) {
			log.error(e.getMessage(),e);
			return false;
		}
		return true;
	}
	//更新用户
	public boolean updateUser(String userName,String avatar,String realName,String phone){
		SysUser su=sysUserService.getUserByName(userName);
		if(null!=su){
			
		
//		SysUser sysUserExcel = new SysUser();
//		if (org.apache.commons.lang.StringUtils.isBlank(sysUserExcel.getPassword())) {
//			// 密码默认为 “Pass_666888”
//			sysUserExcel.setPassword("Pass_666888");
//		}
//		// 密码加密加盐
//		String salt = oConvertUtils.randomGen(8);
//		sysUserExcel.setSalt(salt);
//		sysUserExcel.setUsername(userName);
		su.setRealname(realName);
//		String passwordEncode = PasswordUtil.encrypt(sysUserExcel.getUsername(), sysUserExcel.getPassword(), salt);
//		sysUserExcel.setPassword(passwordEncode);
		su.setDelFlag(0);
		if(!StringUtil.isNullOrEmpty(avatar)){
			su.setAvatar(avatar);
		}

		if(StringUtil.isNullOrEmpty(su.getDingId())){
			su.setDingId(userName);
		}
		if(!StringUtil.isNullOrEmpty(phone)){
			su.setPhone(phone);
		}

		su.setStatus(1);
		sysUserService.updateById(su);
		}else{
			log.error("用户不存在发生什么了");
			return false;
		}
//		try {
////			sysUserService.save(sysUserExcel);
//			QueryWrapper<SysRole> queryWrapper=new QueryWrapper<SysRole>();
//			queryWrapper.eq("role_code","common");
//			List<SysRole> pageList = sysRoleService.list(queryWrapper);
//			String roleCode="";
//			if(null!=pageList&&pageList.size()>0){
//				roleCode=pageList.get(0).getId();
//				sysUserService.addUserWithRole(sysUserExcel, roleCode);
//				LambdaQueryWrapper<SysDepart> queryDep = new LambdaQueryWrapper<SysDepart>();
//				queryDep.eq(SysDepart::getParentId, "").or().isNull(SysDepart::getParentId);
//				queryDep.orderByDesc(SysDepart::getOrgCode);
//				List<SysDepart> departList = sysDepartService.list(queryDep);
//				String selectedDeparts="";
//				if(null!=departList&&departList.size()>0){
//					selectedDeparts=departList.get(0).getId();
//				}
//				sysUserService.addUserWithDepart(sysUserExcel, selectedDeparts);
//			}else{
//				log.info("common角色不存");
//			}
//
//		} catch (Exception e) {
//
//		}
		return true;
	}

	@Override
	public void updateSysAnnounReadFlag(String busType, String busId) {
		SysAnnouncement announcement = sysAnnouncementMapper.selectOne(new QueryWrapper<SysAnnouncement>().eq("bus_type",busType).eq("bus_id",busId));
		if(announcement != null){
			LoginUser sysUser = (LoginUser)SecurityUtils.getSubject().getPrincipal();
			String userId = sysUser.getId();
			LambdaUpdateWrapper<SysAnnouncementSend> updateWrapper = new UpdateWrapper().lambda();
			updateWrapper.set(SysAnnouncementSend::getReadFlag, CommonConstant.HAS_READ_FLAG);
			updateWrapper.set(SysAnnouncementSend::getReadTime, new Date());
			updateWrapper.last("where annt_id ='"+announcement.getId()+"' and user_id ='"+userId+"'");
			SysAnnouncementSend announcementSend = new SysAnnouncementSend();
			sysAnnouncementSendMapper.update(announcementSend, updateWrapper);
		}
	}

	@Override
    public String parseTemplateByCode(String templateCode,Map<String, String> map) {
        List<SysMessageTemplate> sysSmsTemplates = sysMessageTemplateService.selectByCode(templateCode);
        if(sysSmsTemplates==null||sysSmsTemplates.size()==0){
            throw new JeecgBootException("消息模板不存在，模板编码："+templateCode);
        }
        SysMessageTemplate sysSmsTemplate = sysSmsTemplates.get(0);
        //模板内容
        String content = sysSmsTemplate.getTemplateContent();
        if(map!=null) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                String str = "${" + entry.getKey() + "}";
                content = content.replace(str, entry.getValue());
            }
        }
        return content;
    }

	@Override
	public void sendSysAnnouncement(String fromUser, String toUser,String title,Map<String, String> map, String templateCode) {
		List<SysMessageTemplate> sysSmsTemplates = sysMessageTemplateService.selectByCode(templateCode);
        if(sysSmsTemplates==null||sysSmsTemplates.size()==0){
            throw new JeecgBootException("消息模板不存在，模板编码："+templateCode);
        }
		SysMessageTemplate sysSmsTemplate = sysSmsTemplates.get(0);
		//模板标题
		title = title==null?sysSmsTemplate.getTemplateName():title;
		//模板内容
		String content = sysSmsTemplate.getTemplateContent();
		if(map!=null) {
			for (Map.Entry<String, String> entry : map.entrySet()) {
				String str = "${" + entry.getKey() + "}";
				title = title.replace(str, entry.getValue());
				content = content.replace(str, entry.getValue());
			}
		}

		SysAnnouncement announcement = new SysAnnouncement();
		announcement.setTitile(title);
		announcement.setMsgContent(content);
		announcement.setSender(fromUser);
		announcement.setPriority(CommonConstant.PRIORITY_M);
		announcement.setMsgType(CommonConstant.MSG_TYPE_UESR);
		announcement.setSendStatus(CommonConstant.HAS_SEND);
		announcement.setSendTime(new Date());
		announcement.setMsgCategory(CommonConstant.MSG_CATEGORY_2);
		announcement.setDelFlag(String.valueOf(CommonConstant.DEL_FLAG_0));
		sysAnnouncementMapper.insert(announcement);
		// 2.插入用户通告阅读标记表记录
		String userId = toUser;
		String[] userIds = userId.split(",");
		String anntId = announcement.getId();
		for(int i=0;i<userIds.length;i++) {
			if(oConvertUtils.isNotEmpty(userIds[i])) {
				SysUser sysUser = userMapper.getUserByName(userIds[i]);
				if(sysUser==null) {
					continue;
				}
				SysAnnouncementSend announcementSend = new SysAnnouncementSend();
				announcementSend.setAnntId(anntId);
				announcementSend.setUserId(sysUser.getId());
				announcementSend.setReadFlag(CommonConstant.NO_READ_FLAG);
				sysAnnouncementSendMapper.insert(announcementSend);
				JSONObject obj = new JSONObject();
				obj.put(WebsocketConst.MSG_CMD, WebsocketConst.CMD_USER);
				obj.put(WebsocketConst.MSG_USER_ID, sysUser.getId());
				obj.put(WebsocketConst.MSG_ID, announcement.getId());
				obj.put(WebsocketConst.MSG_TXT, announcement.getTitile());
				webSocket.sendOneMessage(sysUser.getId(), obj.toJSONString());
			}
		}
	}

	@Override
	public void sendSysAnnouncement(String fromUser, String toUser, String title, Map<String, String> map, String templateCode, String busType, String busId) {
		List<SysMessageTemplate> sysSmsTemplates = sysMessageTemplateService.selectByCode(templateCode);
		if(sysSmsTemplates==null||sysSmsTemplates.size()==0){
			throw new JeecgBootException("消息模板不存在，模板编码："+templateCode);
		}
		SysMessageTemplate sysSmsTemplate = sysSmsTemplates.get(0);
		//模板标题
		title = title==null?sysSmsTemplate.getTemplateName():title;
		//模板内容
		String content = sysSmsTemplate.getTemplateContent();
		if(map!=null) {
			for (Map.Entry<String, String> entry : map.entrySet()) {
				String str = "${" + entry.getKey() + "}";
				title = title.replace(str, entry.getValue());
				content = content.replace(str, entry.getValue());
			}
		}
		SysAnnouncement announcement = new SysAnnouncement();
		announcement.setTitile(title);
		announcement.setMsgContent(content);
		announcement.setSender(fromUser);
		announcement.setPriority(CommonConstant.PRIORITY_M);
		announcement.setMsgType(CommonConstant.MSG_TYPE_UESR);
		announcement.setSendStatus(CommonConstant.HAS_SEND);
		announcement.setSendTime(new Date());
		announcement.setMsgCategory(CommonConstant.MSG_CATEGORY_2);
		announcement.setDelFlag(String.valueOf(CommonConstant.DEL_FLAG_0));
		announcement.setBusId(busId);
		announcement.setBusType(busType);
		announcement.setOpenType(SysAnnmentTypeEnum.getByType(busType).getOpenType());
		announcement.setOpenPage(SysAnnmentTypeEnum.getByType(busType).getOpenPage());
		sysAnnouncementMapper.insert(announcement);
		// 2.插入用户通告阅读标记表记录
		String userId = toUser;
		String[] userIds = userId.split(",");
		String anntId = announcement.getId();
		for(int i=0;i<userIds.length;i++) {
			if(oConvertUtils.isNotEmpty(userIds[i])) {
				SysUser sysUser = userMapper.getUserByName(userIds[i]);
				if(sysUser==null) {
					continue;
				}
				SysAnnouncementSend announcementSend = new SysAnnouncementSend();
				announcementSend.setAnntId(anntId);
				announcementSend.setUserId(sysUser.getId());
				announcementSend.setReadFlag(CommonConstant.NO_READ_FLAG);
				sysAnnouncementSendMapper.insert(announcementSend);
				JSONObject obj = new JSONObject();
				obj.put(WebsocketConst.MSG_CMD, WebsocketConst.CMD_USER);
				obj.put(WebsocketConst.MSG_USER_ID, sysUser.getId());
				obj.put(WebsocketConst.MSG_ID, announcement.getId());
				obj.put(WebsocketConst.MSG_TXT, announcement.getTitile());
				webSocket.sendOneMessage(sysUser.getId(), obj.toJSONString());
			}
		}
	}

	/**
	 * 获取数据库类型
	 * @param dataSource
	 * @return
	 * @throws SQLException
	 */
	private String getDatabaseTypeByDataSource(DataSource dataSource) throws SQLException{
		if("".equals(DB_TYPE)) {
			Connection connection = dataSource.getConnection();
			try {
				DatabaseMetaData md = connection.getMetaData();
				String dbType = md.getDatabaseProductName().toLowerCase();
				if(dbType.indexOf("mysql")>=0) {
					DB_TYPE = DataBaseConstant.DB_TYPE_MYSQL;
				}else if(dbType.indexOf("oracle")>=0) {
					DB_TYPE = DataBaseConstant.DB_TYPE_ORACLE;
				}else if(dbType.indexOf("sqlserver")>=0||dbType.indexOf("sql server")>=0) {
					DB_TYPE = DataBaseConstant.DB_TYPE_SQLSERVER;
				}else if(dbType.indexOf("postgresql")>=0) {
					DB_TYPE = DataBaseConstant.DB_TYPE_POSTGRESQL;
				}else {
					throw new JeecgBootException("数据库类型:["+dbType+"]不识别!");
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}finally {
				connection.close();
			}
		}
		return DB_TYPE;
		
	}

	@Override
	public List<DictModel> queryAllDict() {
		// 查询并排序
		QueryWrapper<SysDict> queryWrapper = new QueryWrapper<SysDict>();
		queryWrapper.orderByAsc("create_time");
		List<SysDict> dicts = sysDictService.list(queryWrapper);
		// 封装成 model
		List<DictModel> list = new ArrayList<DictModel>();
		for (SysDict dict : dicts) {
			list.add(new DictModel(dict.getDictCode(), dict.getDictName()));
		}

		return list;
	}

	@Override
	public List<SysCategoryModel> queryAllDSysCategory() {
		List<SysCategory> ls = categoryMapper.selectList(null);
		List<SysCategoryModel> res = oConvertUtils.entityListToModelList(ls,SysCategoryModel.class);
		return res;
	}

	@Override
	public List<DictModel> queryFilterTableDictInfo(String table, String text, String code, String filterSql) {
		return sysDictService.queryTableDictItemsByCodeAndFilter(table,text,code,filterSql);
	}

	@Override
	public List<String> queryTableDictByKeys(String table, String text, String code, String[] keyArray) {
		return sysDictService.queryTableDictByKeys(table,text,code,keyArray);
	}

	@Override
	public List<ComboModel> queryAllUser() {
		List<ComboModel> list = new ArrayList<ComboModel>();
		List<SysUser> userList = userMapper.selectList(new QueryWrapper<SysUser>().eq("status",1).eq("del_flag",0));
		for(SysUser user : userList){
			ComboModel model = new ComboModel();
			model.setTitle(user.getRealname());
			model.setId(user.getId());
			model.setUsername(user.getUsername());
			list.add(model);
		}
		return list;
	}

    @Override
    public JSONObject queryAllUser(String[] userIds,int pageNo,int pageSize) {
		JSONObject json = new JSONObject();
		QueryWrapper<SysUser> queryWrapper = new QueryWrapper<SysUser>().eq("status",1).eq("del_flag",0);
        List<ComboModel> list = new ArrayList<ComboModel>();
		Page<SysUser> page = new Page<SysUser>(pageNo, pageSize);
		IPage<SysUser> pageList = userMapper.selectPage(page, queryWrapper);
        for(SysUser user : pageList.getRecords()){
            ComboModel model = new ComboModel();
            model.setUsername(user.getUsername());
            model.setTitle(user.getRealname());
            model.setId(user.getId());
            model.setEmail(user.getEmail());
            if(oConvertUtils.isNotEmpty(userIds)){
                for(int i = 0; i<userIds.length;i++){
                    if(userIds[i].equals(user.getId())){
                        model.setChecked(true);
                    }
                }
            }
            list.add(model);
        }
		json.put("list",list);
        json.put("total",pageList.getTotal());
        return json;
    }

    @Override
    public List<JSONObject> queryAllUser(Wrapper wrapper) {
        //noinspection unchecked
        return JSON.parseArray(JSON.toJSONString(userMapper.selectList(wrapper))).toJavaList(JSONObject.class);
    }

    @Override
	public List<ComboModel> queryAllRole() {
		List<ComboModel> list = new ArrayList<ComboModel>();
		List<SysRole> roleList = roleMapper.selectList(new QueryWrapper<SysRole>());
		for(SysRole role : roleList){
			ComboModel model = new ComboModel();
			model.setTitle(role.getRoleName());
			model.setId(role.getId());
			list.add(model);
		}
		return list;
	}

    @Override
    public List<ComboModel> queryAllRole(String[] roleIds) {
        List<ComboModel> list = new ArrayList<ComboModel>();
        List<SysRole> roleList = roleMapper.selectList(new QueryWrapper<SysRole>());
        for(SysRole role : roleList){
            ComboModel model = new ComboModel();
            model.setTitle(role.getRoleName());
            model.setId(role.getId());
            model.setRoleCode(role.getRoleCode());
            if(oConvertUtils.isNotEmpty(roleIds)) {
                for (int i = 0; i < roleIds.length; i++) {
                    if (roleIds[i].equals(role.getId())) {
                        model.setChecked(true);
                    }
                }
            }
            list.add(model);
        }
        return list;
    }
	@Override
	public boolean checkAdmin(String username){
		List<String> roles=getRolesByUsername(username);
		List<String> rel=roles.stream().filter(item->"admin".equals(item)).collect(Collectors.toList());
		if(null!=rel&&rel.size()>0){
			return true;
		}else{
			return false;
		}
	}
	@Override
	public List<String> getRoleIdsByUsername(String username) {
		return sysUserRoleMapper.getRoleIdByUserName(username);
	}

	@Override
	public String getDepartIdsByOrgCode(String orgCode) {
		return departMapper.queryDepartIdByOrgCode(orgCode);
	}

	@Override
	public DictModel getParentDepartId(String departId) {
		SysDepart depart = departMapper.getParentDepartId(departId);
		DictModel model = new DictModel(depart.getId(),depart.getParentId());
		return model;
	}

	@Override
	public List<SysDepartModel> getAllSysDepart() {
		List<SysDepartModel> departModelList = new ArrayList<SysDepartModel>();
		List<SysDepart> departList = departMapper.selectList(new QueryWrapper<SysDepart>().eq("del_flag","0"));
		for(SysDepart depart : departList){
			SysDepartModel model = new SysDepartModel();
			BeanUtils.copyProperties(depart,model);
			departModelList.add(model);
		}
		return departModelList;
	}

	@Override
	public DynamicDataSourceModel getDynamicDbSourceById(String dbSourceId) {
		SysDataSource dbSource = dataSourceService.getById(dbSourceId);
		return new DynamicDataSourceModel(dbSource);
	}

	@Override
	public DynamicDataSourceModel getDynamicDbSourceByCode(String dbSourceCode) {
		SysDataSource dbSource = dataSourceService.getOne(new LambdaQueryWrapper<SysDataSource>().eq(SysDataSource::getCode, dbSourceCode));
		return new DynamicDataSourceModel(dbSource);
	}

	@Override
	public void updateOrgName(String orgName) {
		if(!StringUtil.isNullOrEmpty(orgName)){
			QueryWrapper<SysDepart> deptQueryWrapper=new QueryWrapper<SysDepart>();
			deptQueryWrapper.eq("org_type","1");
			List<SysDepart> departs=sysDepartService.list(deptQueryWrapper);
			if(null!=departs&&departs.size()>0){
				for(SysDepart sysDepart:departs){
					sysDepart.setDepartName(orgName);
					sysDepartService.updateById(sysDepart);
				}
			}
		}
	}

	@Override
	public List<String> getDeptHeadByDepId(String deptId) {

		List<SysUser> userList = userMapper.selectList(new QueryWrapper<SysUser>().like("depart_ids",deptId).eq("status",1).eq("del_flag",0));
		List<String> list = new ArrayList<>();
		for(SysUser user : userList){
			list.add(user.getUsername());
		}
		return list;
	}
	@Override
	public LoginUser getByPwdwId(String pwdwId) {
		LoginUser rel=null;
		List<SysUser> userList = userMapper.selectList(new QueryWrapper<SysUser>().eq("pwdw_id",pwdwId).eq("del_flag",0));
		List<LoginUser> loginUsers = new ArrayList<>();
		if(null!=userList&&userList.size()>0){
			rel=new LoginUser();
			BeanUtils.copyProperties(userList.get(0), rel);
		}
		return rel;
	}



	@Override
	public void createPwdwUser(String userName,String realName,String pwdwId,String regionCode) {
		SysUser su=sysUserService.getUserByName(userName);
		if(null!=su){
			return;
		}
		SysUser sysUserExcel = new SysUser();
		if (org.apache.commons.lang.StringUtils.isBlank(sysUserExcel.getPassword())) {
			// 密码默认为 “123456”
			sysUserExcel.setPassword("Pass_666888");
		}
		// 密码加密加盐
		String salt = oConvertUtils.randomGen(8);
		sysUserExcel.setSalt(salt);
		sysUserExcel.setUsername(userName);
		sysUserExcel.setRealname(realName);
		String passwordEncode = PasswordUtil.encrypt(sysUserExcel.getUsername(), sysUserExcel.getPassword(), salt);
		sysUserExcel.setPassword(passwordEncode);
		sysUserExcel.setUserType("pwdw");
		sysUserExcel.setPwdwId(pwdwId);
		sysUserExcel.setDelFlag(0);
		sysUserExcel.setRegion(regionCode);
		sysUserExcel.setStatus(1);
		sysUserExcel.setUserIdentity(1);

		try {
//			sysUserService.save(sysUserExcel);
			QueryWrapper<SysRole> queryWrapper=new QueryWrapper<SysRole>();
			queryWrapper.eq("role_code","pwdw");
			List<SysRole> pageList = sysRoleService.list(queryWrapper);
			String roleCode="";
			if(null!=pageList&&pageList.size()>0){
				roleCode=pageList.get(0).getId();
				sysUserService.addUserWithRole(sysUserExcel, roleCode);
				LambdaQueryWrapper<SysDepart> queryDep = new LambdaQueryWrapper<SysDepart>();
				queryDep.eq(SysDepart::getParentId, "").or().isNull(SysDepart::getParentId);
				queryDep.orderByDesc(SysDepart::getOrgCode);
				List<SysDepart> departList = sysDepartService.list(queryDep);
				String selectedDeparts="";
				if(null!=departList&&departList.size()>0){
					selectedDeparts=departList.get(0).getId();
				}
				sysUserService.addUserWithDepart(sysUserExcel, selectedDeparts);
			}else{
				log.info("pwdw角色不存");
			}

		} catch (Exception e) {

		}
	}
	@Override
	public String upload(MultipartFile file,String bizPath,String uploadType) {
		String url = "";
		if(CommonConstant.UPLOAD_TYPE_MINIO.equals(uploadType)){
			url = MinioUtil.upload(file,bizPath);
		}else{
			url = OssBootUtil.upload(file,bizPath);
		}
		return url;
	}

	@Override
	public String upload(MultipartFile file, String bizPath, String uploadType, String customBucket) {
		String url = "";
		if(CommonConstant.UPLOAD_TYPE_MINIO.equals(uploadType)){
			url = MinioUtil.upload(file,bizPath,customBucket);
		}else{
			url = OssBootUtil.upload(file,bizPath,customBucket);
		}
		return url;
	}

	@Override
	public void viewAndDownload(String filePath, String uploadpath, String uploadType, HttpServletResponse response) {
		InputStream inputStream = null;
		OutputStream outputStream = null;
		try {
			if(filePath.startsWith("http")){
				String fileName = filePath.substring(filePath.lastIndexOf("/")+1);
				if(CommonConstant.UPLOAD_TYPE_MINIO.equals(uploadType)){
					String bucketName = filePath.replace(MinioUtil.getMinioUrl(),"").split("/")[0];
					String objectName = filePath.replace(MinioUtil.getMinioUrl()+bucketName,"");
					inputStream = MinioUtil.getMinioFile(bucketName,objectName);
					if(inputStream == null){
						bucketName = CommonConstant.UPLOAD_CUSTOM_BUCKET;
						objectName = filePath.replace(OssBootUtil.getStaticDomain()+"/","");
						inputStream = OssBootUtil.getOssFile(objectName,bucketName);
					}
				}else{
					String bucketName = CommonConstant.UPLOAD_CUSTOM_BUCKET;
					String objectName = filePath.replace(OssBootUtil.getStaticDomain()+"/","");
					inputStream = OssBootUtil.getOssFile(objectName,bucketName);
					if(inputStream == null){
						bucketName = filePath.replace(MinioUtil.getMinioUrl(),"").split("/")[0];
						objectName = filePath.replace(MinioUtil.getMinioUrl()+bucketName,"");
						inputStream = MinioUtil.getMinioFile(bucketName,objectName);
					}
				}
				response.addHeader("Content-Disposition", "attachment;fileName=" + new String(fileName.getBytes("UTF-8"),"iso-8859-1"));
			}else{
				// 本地文件处理
				filePath = filePath.replace("..", "");
				if (filePath.endsWith(",")) {
					filePath = filePath.substring(0, filePath.length() - 1);
				}
				String fullPath = uploadpath + File.separator + filePath;
				String downloadFilePath = uploadpath + File.separator + fullPath;
				File file = new File(downloadFilePath);
				inputStream = new BufferedInputStream(new FileInputStream(fullPath));
				response.addHeader("Content-Disposition", "attachment;fileName=" + new String(file.getName().getBytes("UTF-8"),"iso-8859-1"));
			}
			response.setContentType("application/force-download");// 设置强制下载不打开
			outputStream = response.getOutputStream();
			if(inputStream != null){
				byte[] buf = new byte[1024];
				int len;
				while ((len = inputStream.read(buf)) > 0) {
					outputStream.write(buf, 0, len);
				}
				response.flushBuffer();
			}
		} catch (IOException e) {
			response.setStatus(404);
			log.error("预览文件失败" + e.getMessage());
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					log.error(e.getMessage(), e);
				}
			}
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					log.error(e.getMessage(), e);
				}
			}
		}
	}

	@Override
	public void sendWebSocketMsg(String[] userIds, String cmd) {
		JSONObject obj = new JSONObject();
		obj.put(WebsocketConst.MSG_CMD, cmd);
		webSocket.sendMoreMessage(userIds, obj.toJSONString());
	}

	@Override
	public List<LoginUser> queryAllUserByIds(String[] userIds) {
		QueryWrapper<SysUser> queryWrapper = new QueryWrapper<SysUser>().eq("status",1).eq("del_flag",0);
		queryWrapper.in("id",userIds);
		List<LoginUser> loginUsers = new ArrayList<>();
		List<SysUser> sysUsers = userMapper.selectList(queryWrapper);
		for (SysUser user:sysUsers) {
			LoginUser loginUser=new LoginUser();
			BeanUtils.copyProperties(user, loginUser);
			loginUsers.add(loginUser);
		}
		return loginUsers;
	}

	/**
	 * 推送签到人员信息
	 * @param userId
	 */
	@Override
	public void meetingSignWebsocket(String userId) {
		JSONObject obj = new JSONObject();
		obj.put(WebsocketConst.MSG_CMD, WebsocketConst.CMD_SIGN);
		obj.put(WebsocketConst.MSG_USER_ID,userId);
		//TODO 目前全部推送，后面修改
		webSocket.sendAllMessage(obj.toJSONString());
	}

	@Override
	public List<LoginUser> queryUserByNames(String[] userNames) {
		QueryWrapper<SysUser> queryWrapper = new QueryWrapper<SysUser>().eq("status",1).eq("del_flag",0);
		queryWrapper.in("username",userNames);
		List<LoginUser> loginUsers = new ArrayList<>();
		List<SysUser> sysUsers = userMapper.selectList(queryWrapper);
		for (SysUser user:sysUsers) {
			LoginUser loginUser=new LoginUser();
			BeanUtils.copyProperties(user, loginUser);
			loginUsers.add(loginUser);
		}
		return loginUsers;
	}

	@Override
	public String getDingIdByName(String realName) {
		QueryWrapper<SysUser> queryWrapper = new QueryWrapper<SysUser>();
		queryWrapper.eq("realname",realName);
		List<LoginUser> loginUsers = new ArrayList<>();
		List<SysUser> sysUsers = userMapper.selectList(queryWrapper);
		String dingId=null;
		if (null!=sysUsers&& sysUsers.size()>0) {
			SysUser loginUser=sysUsers.get(0);
			dingId=loginUser.getDingId();

		}
		return dingId;
	}
	@Override
	public String getUserIdByName(String realName) {
		QueryWrapper<SysUser> queryWrapper = new QueryWrapper<SysUser>();
		queryWrapper.eq("realname",realName);
		List<LoginUser> loginUsers = new ArrayList<>();
		List<SysUser> sysUsers = userMapper.selectList(queryWrapper);
		String userId=null;
		if (null!=sysUsers&& sysUsers.size()>0) {
			SysUser loginUser=sysUsers.get(0);
			userId=loginUser.getId();

		}
		return userId;
	}
	@Override
	public String getRealNameByDingId(String dingId) {
		QueryWrapper<SysUser> queryWrapper = new QueryWrapper<SysUser>();
		queryWrapper.eq("ding_id",dingId);
		List<LoginUser> loginUsers = new ArrayList<>();
		List<SysUser> sysUsers = userMapper.selectList(queryWrapper);
		String realName=null;
		if (null!=sysUsers&& sysUsers.size()>0) {
			SysUser loginUser=sysUsers.get(0);
			realName=loginUser.getRealname();

		}
		return realName;
	}
	@Override
	public String getUserIdByDingId(String dingId) {
		QueryWrapper<SysUser> queryWrapper = new QueryWrapper<SysUser>();
		queryWrapper.eq("ding_id",dingId);
		List<LoginUser> loginUsers = new ArrayList<>();
		List<SysUser> sysUsers = userMapper.selectList(queryWrapper);
		String userId=null;
		if (null!=sysUsers&& sysUsers.size()>0) {
			SysUser loginUser=sysUsers.get(0);
			userId=loginUser.getId();

		}
		return userId;
	}

	public  List<JSONObject> parseDictTextList(Object para) {
		if (null!=para&&para instanceof List) {
			List<JSONObject> items = new ArrayList<>();
			for (Object record : (List<Object>)para) {
//				log.info("开始序列化");
				ObjectMapper mapper = new ObjectMapper();
				String json="{}";
				try {
					//解决@JsonFormat注解解析不了的问题详见SysAnnouncement类的@JsonFormat
					json = mapper.writeValueAsString(record);
				} catch (JsonProcessingException e) {
					log.error("json解析失败"+e.getMessage(),e);
				}
				JSONObject item = JSONObject.parseObject(json);
//				log.info("结束序列化");
				//update-begin--Author:scott -- Date:20190603 ----for：解决继承实体字段无法翻译问题------
				//for (Field field : record.getClass().getDeclaredFields()) {
				for (Field field : oConvertUtils.getAllFields(record)) {
					//update-end--Author:scott  -- Date:20190603 ----for：解决继承实体字段无法翻译问题------
					if (field.getAnnotation(Dict.class) != null) {
						String code = field.getAnnotation(Dict.class).dicCode();
						String text = field.getAnnotation(Dict.class).dicText();
						String table = field.getAnnotation(Dict.class).dictTable();
						String key = String.valueOf(item.get(field.getName()));

//						log.info("翻译前"+code+"|"+text+"|"+table+"|"+key);
						//翻译字典值对应的txt
						String textValue = translateDictValue(code, text, table, key);
//						log.info("翻译后"+code+"|"+text+"|"+table+"|"+key);
						log.debug(" 字典Val : "+ textValue);
						log.debug(" __翻译字典字段__ "+field.getName() + CommonConstant.DICT_TEXT_SUFFIX+"： "+ textValue);
						item.put(field.getName() + CommonConstant.DICT_TEXT_SUFFIX, textValue);
					}
					//date类型默认转换string格式化日期
					if (field.getType().getName().equals("java.util.Date")&&field.getAnnotation(JsonFormat.class)==null&&item.get(field.getName())!=null){
						SimpleDateFormat aDate=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						item.put(field.getName(), aDate.format(new Date((Long) item.get(field.getName()))));
					}
				}
				items.add(item);
			}

			return items;

		}else{
			return null;
		}

	}

	public  JSONObject parseDictTextOjb(Object record) {
		if (null!=record) {

			ObjectMapper mapper = new ObjectMapper();
			String json = "{}";
			try {
				//解决@JsonFormat注解解析不了的问题详见SysAnnouncement类的@JsonFormat
				json = mapper.writeValueAsString(record);
			} catch (JsonProcessingException e) {
				log.error("json解析失败" + e.getMessage(), e);
			}
			JSONObject item = JSONObject.parseObject(json);
			//update-begin--Author:scott -- Date:20190603 ----for：解决继承实体字段无法翻译问题------
			//for (Field field : record.getClass().getDeclaredFields()) {
			for (Field field : oConvertUtils.getAllFields(record)) {
				//update-end--Author:scott  -- Date:20190603 ----for：解决继承实体字段无法翻译问题------
				if (field.getAnnotation(Dict.class) != null) {
					String code = field.getAnnotation(Dict.class).dicCode();
					String text = field.getAnnotation(Dict.class).dicText();
					String table = field.getAnnotation(Dict.class).dictTable();
					String key = String.valueOf(item.get(field.getName()));

					//翻译字典值对应的txt
					String textValue = translateDictValue(code, text, table, key);

					log.debug(" 字典Val : " + textValue);
					log.debug(" __翻译字典字段__ " + field.getName() + CommonConstant.DICT_TEXT_SUFFIX + "： " + textValue);
					item.put(field.getName() + CommonConstant.DICT_TEXT_SUFFIX, textValue);
				}
				//date类型默认转换string格式化日期
				if (field.getType().getName().equals("java.util.Date") && field.getAnnotation(JsonFormat.class) == null && item.get(field.getName()) != null) {
					SimpleDateFormat aDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					item.put(field.getName(), aDate.format(new Date((Long) item.get(field.getName()))));
				}
			}
			return item;
		}else{
			return null;
		}
	}

	/**
	 *  翻译字典文本
	 * @param code
	 * @param text
	 * @param table
	 * @param key
	 * @return
	 */
	private String translateDictValue(String code, String text, String table, String key) {
		if(oConvertUtils.isEmpty(key)) {
			return null;
		}
		StringBuffer textValue=new StringBuffer();
		String[] keys = key.split(",");
		for (String k : keys) {
			String tmpValue = null;
			log.debug(" 字典 key : "+ k);
			if (k.trim().length() == 0) {
				continue; //跳过循环
			}
			if (!StringUtils.isEmpty(table)){
				log.debug("--DictAspect------dicTable="+ table+" ,dicText= "+text+" ,dicCode="+code);
				tmpValue= dictService.queryTableDictTextByKey(table,text,code,k.trim());
			}else {
				tmpValue = dictService.queryDictTextByKey(code, k.trim());
			}

			if (tmpValue != null) {
				if (!"".equals(textValue.toString())) {
					textValue.append(",");
				}
				textValue.append(tmpValue);
			}

		}
		return textValue.toString();
	}

}