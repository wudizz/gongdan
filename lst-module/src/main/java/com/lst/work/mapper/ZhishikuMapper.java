package com.lst.work.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.lst.work.entity.Zhishiku;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 知识库
 * @Author: jeecg-boot
 * @Date:   2022-10-16
 * @Version: V1.0
 */
public interface ZhishikuMapper extends BaseMapper<Zhishiku> {

}
