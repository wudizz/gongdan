package com.lst.work.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.lst.work.entity.DeadlineMsg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 超时提醒
 * @Author: jeecg-boot
 * @Date:   2022-09-03
 * @Version: V1.0
 */
public interface DeadlineMsgMapper extends BaseMapper<DeadlineMsg> {

}
