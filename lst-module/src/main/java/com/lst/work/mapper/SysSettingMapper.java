package com.lst.work.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.lst.work.entity.SysSetting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 系统设置
 * @Author: jeecg-boot
 * @Date:   2022-08-30
 * @Version: V1.0
 */
public interface SysSettingMapper extends BaseMapper<SysSetting> {

}
