package com.lst.work.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.lst.work.entity.WorkOrderRealign;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 工单分配记录
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
public interface WorkOrderRealignMapper extends BaseMapper<WorkOrderRealign> {

}
