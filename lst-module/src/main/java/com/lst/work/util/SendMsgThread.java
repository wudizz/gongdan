package com.lst.work.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lst.dingtalk.Sample;
import com.lst.work.entity.DingMsg;
import com.lst.work.entity.WorkOrder;
import com.lst.work.entity.WorkOrderTypeDiyColumn;
import com.lst.work.entity.WorkOrderTypeUser;
import com.lst.work.service.IDingMsgService;
import com.lst.work.service.IWorkOrderService;
import com.lst.work.service.IWorkOrderTypeDiyColumnService;
import com.lst.work.service.IWorkOrderTypeUserService;
import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.vo.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.text.SimpleDateFormat;
import java.util.*;


@Slf4j
public class SendMsgThread extends  Thread{
    IWorkOrderService workOrderService;
    ISysBaseAPI sysBaseAPI;
    IWorkOrderTypeUserService workOrderTypeUserService;
    IDingMsgService dingMsgService;
    IWorkOrderTypeDiyColumnService workOrderTypeDiyColumnService;

    String  workOrderId="";
    String msgType="";//处理、待处理、评价 完成
    String  domain="";
    String agentId="";
    String  appKey="";
    String appSecret="";
    int  chaoshiHours=0;
    String dingMsgType="";
    int  warnTimes=1;
    public SendMsgThread(String tmp,String msgType,String domain,String agentId,String appKey,String appSecret
    ,IWorkOrderService workOrderService,ISysBaseAPI sysBaseAPI,IWorkOrderTypeUserService workOrderTypeUserService,IDingMsgService dingMsgService,IWorkOrderTypeDiyColumnService workOrderTypeDiyColumnService,int chaoshiHours,String dingMsgType,int warnTimes){
        this.workOrderTypeDiyColumnService=workOrderTypeDiyColumnService;
        this.workOrderService=workOrderService;
        this.sysBaseAPI=sysBaseAPI;
        this.workOrderTypeUserService=workOrderTypeUserService;
        this.dingMsgService=dingMsgService;
        this.workOrderId=tmp;
        this.msgType=msgType;
        this.domain=domain;
        this.agentId=agentId;
        this.appKey=appKey;
        this.appSecret=appSecret;
        this.chaoshiHours=chaoshiHours;
        this.dingMsgType=dingMsgType;
        this.warnTimes=warnTimes;
    }
    public void run(){
        try{
            String title="您有新的[待办]工单";
            String useridsList="";
            String urlParam="";
            if(StringUtil.isNullOrEmpty(workOrderId)){
                log.error("workOrderId为空");
                return;
            }
            String content="";
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            content="亲,来工单了,送达时间"+sdf.format(new Date())+",点击即可查看办理！";
            WorkOrder workOrder=workOrderService.getById(workOrderId);
            JSONObject jsonObject=sysBaseAPI.parseDictTextOjb(workOrder);
            if(WorkConst.MSG_TYPE_PAIDAN.equals(msgType)||WorkConst.ORDER_STATUS_ALIGN.equals(workOrder.getStatus())){
                if(null!=workOrder&& !StringUtil.isNullOrEmpty(workOrder.getType())){
                    List<WorkOrderTypeUser> workOrderTypeUsers=null;
                    QueryWrapper<WorkOrderTypeUser> workOrderTypeUserQueryWrapper=new QueryWrapper<WorkOrderTypeUser>();
                    workOrderTypeUserQueryWrapper.eq("work_order_type",workOrder.getType());
                    workOrderTypeUsers=workOrderTypeUserService.list(workOrderTypeUserQueryWrapper);
                    if(null!=workOrderTypeUsers&&workOrderTypeUsers.size()>0){
                        for(WorkOrderTypeUser workOrderTypeUser:workOrderTypeUsers){
                            if(!StringUtil.isNullOrEmpty(workOrderTypeUser.getAlignUser())){
                                LoginUser loginUser=sysBaseAPI.getUserById(workOrderTypeUser.getAlignUser());
                                useridsList+=loginUser.getUsername()+",";
                            }

                        }
                        if(!StringUtil.isNullOrEmpty(useridsList)&&useridsList.endsWith(",")){
                            useridsList=useridsList.substring(0,useridsList.length()-1);
                            urlParam="work_order_detail?showHome=1&workOrderId="+workOrder.getId()+"&operType="+"edit";
                        }
                    }
                }
                title="您有新的[待派]工单";
            }
            if(WorkConst.MSG_TYPE_GET.equals(msgType)||WorkConst.ORDER_STATUS_GET.equals(workOrder.getStatus())){
                title="您有新的[待接单]工单";
                String userId=workOrder.getToUser();
                if(!StringUtil.isNullOrEmpty(userId)){
                    LoginUser loginUser=sysBaseAPI.getUserById(userId);
                    if(null!=loginUser){
                        useridsList=loginUser.getUsername();
                    }
                }
//					 useridsList=workOrder.getToUser();
                urlParam="work_order_detail?showHome=1&workOrderId="+workOrder.getId()+"&operType="+"edit";
            }
            if(WorkConst.MSG_TYPE_CHULI.equals(msgType)||WorkConst.ORDER_STATUS_HANDLE.equals(workOrder.getStatus())){
                title="您有新的[待处理]工单";
                String userId=workOrder.getToUser();
                if(!StringUtil.isNullOrEmpty(userId)){
                    LoginUser loginUser=sysBaseAPI.getUserById(userId);
                    if(null!=loginUser){
                        useridsList=loginUser.getUsername();
                    }
                }
//					 useridsList=workOrder.getToUser();
                urlParam="work_order_detail?showHome=1&workOrderId="+workOrder.getId()+"&operType="+"edit";
            }

            if(WorkConst.MSG_TYPE_QUEREN.equals(msgType)||WorkConst.ORDER_STATUS_SCORE.equals(workOrder.getStatus())){
                title="您有新的[待确认]工单";
//					 useridsList=workOrder.getFromUser();
                String userId=workOrder.getFromUser();
                if(!StringUtil.isNullOrEmpty(userId)){
                    LoginUser loginUser=sysBaseAPI.getUserById(userId);
                    if(null!=loginUser){
                        useridsList=loginUser.getUsername();
                    }
                }
                urlParam="work_order_detail?showHome=1&workOrderId="+workOrder.getId()+"&operType="+"edit";
            }
            if(WorkConst.MSG_TYPE_DEADLINE_CHAOSONGPAIDAN.equals(msgType)){
                String userId=workOrder.getFromUser();
                if(!StringUtil.isNullOrEmpty(userId)){
                    LoginUser loginUser=sysBaseAPI.getUserById(userId);
                    if(null!=loginUser){
                        useridsList=loginUser.getUsername();
                    }
                }
            }
            if(WorkConst.MSG_TYPE_WANCHENG.equals(msgType)){
                title="您经办的工单已完成，请查阅详情";

                String fromUser="";
                String toUser="";
                String alignUser="";
                if(!StringUtil.isNullOrEmpty(workOrder.getFromUser())){
                    LoginUser loginUser=sysBaseAPI.getUserById(workOrder.getFromUser());
                    if(null!=loginUser){
                        fromUser=loginUser.getUsername();
                    }
                }
                if(!StringUtil.isNullOrEmpty(workOrder.getToUser())){
                    LoginUser loginUser=sysBaseAPI.getUserById(workOrder.getToUser());
                    if(null!=loginUser){
                        toUser=loginUser.getUsername();
                    }
                }
                if(!StringUtil.isNullOrEmpty(workOrder.getFromUser())){
                    LoginUser loginUser=sysBaseAPI.getUserById(workOrder.getAlignUser());
                    if(null!=loginUser){
                        alignUser=loginUser.getUsername();
                    }
                }

                if(!StringUtil.isNullOrEmpty(fromUser)){
                    useridsList+=fromUser+",";
                }
                if(!StringUtil.isNullOrEmpty(fromUser)){
                    useridsList+=toUser+",";
                }
                if(!StringUtil.isNullOrEmpty(fromUser)){
                    useridsList+=alignUser+",";
                }
                if(!StringUtil.isNullOrEmpty(useridsList)&&useridsList.endsWith(",")){
                    useridsList=useridsList.substring(0,useridsList.length()-1);
                }
                urlParam="work_order_detail?showHome=1&workOrderId="+workOrder.getId()+"&operType="+"read";
            }
            if(WorkConst.MSG_TYPE_DEADLINE_CHAOSONGPAIDAN.equals(msgType)){
                String chaoshiType="";
                if(chaoshiHours==0) {
                    chaoshiType="已超时";
                }
                if(chaoshiHours>=0) {
                    chaoshiType="已超时"+chaoshiHours+"小时";
                }
                if(chaoshiHours<0) {
                    chaoshiType="即将超时，剩余"+(0-chaoshiHours)+"小时";
                    return;
                }
                String status=workOrder.getStatus();
                String statusTxt="待办";
                if(WorkConst.ORDER_STATUS_ALIGN.equals(status)){
                    statusTxt="待派";
                }
                if(WorkConst.ORDER_STATUS_GET.equals(status)){
                    statusTxt="待接单";
                }
                if(WorkConst.ORDER_STATUS_HANDLE.equals(status)){
                    statusTxt="待处理";
                }
                if(WorkConst.ORDER_STATUS_SCORE.equals(status)){
                    statusTxt="待评价";
                }
                title="【工单超时提醒(抄送)】";
                content="亲,您派出的工单处于"+statusTxt+"环节，"+chaoshiType+",送达时间"+sdf.format(new Date())+",请及时跟进，点击查看工单详情！";
                urlParam="work_order_detail?showHome=1&workOrderId="+workOrder.getId()+"&operType="+"edit";
            }
            if(WorkConst.MSG_TYPE_DEADLINE.equals(msgType)){
                String status=workOrder.getStatus();
                String statusTxt="待办";
                if(WorkConst.ORDER_STATUS_ALIGN.equals(status)){
                    statusTxt="待派";
                }
                if(WorkConst.ORDER_STATUS_GET.equals(status)){
                    statusTxt="待接";
                }
                if(WorkConst.ORDER_STATUS_HANDLE.equals(status)){
                    statusTxt="待处理";
                }
                if(WorkConst.ORDER_STATUS_SCORE.equals(status)){
                    statusTxt="待评价";
                }

                String chaoshiType="";
                if(chaoshiHours==0) {
                    chaoshiType="已超时";
                }
                if(chaoshiHours>=0) {
                    chaoshiType="已超时"+chaoshiHours+"小时";
                }
                if(chaoshiHours<0) {
                    chaoshiType="即将超时，剩余"+(0-chaoshiHours)+"小时";
                }
                title="【工单超时提醒】";
                content="亲,您有一个"+statusTxt+"工单"+chaoshiType+",送达时间"+sdf.format(new Date())+",点击即可查看办理！";
                urlParam="work_order_detail?showHome=1&workOrderId="+workOrder.getId()+"&operType="+"edit";
            }
            String url=domain+urlParam;


            HashMap<String,String> forms=new LinkedHashMap<String,String>();
            forms.put("工单编号:",workOrder.getNumber());
            if(null!=jsonObject.get("type_dictText")){
                forms.put("服务项目:",(String)jsonObject.get("type_dictText"));
            }


            JSONObject relJson=sysBaseAPI.parseDictTextOjb(workOrder);
            //扩展字段展示
            QueryWrapper<WorkOrderTypeDiyColumn> workOrderTypeDiyColumnQueryWrapper=new QueryWrapper<WorkOrderTypeDiyColumn>();
            workOrderTypeDiyColumnQueryWrapper.eq("work_order_type",workOrder.getType());
            workOrderTypeDiyColumnQueryWrapper.eq("is_show","1");
            workOrderTypeDiyColumnQueryWrapper.orderByAsc("order_num");
            List<WorkOrderTypeDiyColumn> workOrderTypeDiyColumns=workOrderTypeDiyColumnService.list(workOrderTypeDiyColumnQueryWrapper);
            if(null!=workOrderTypeDiyColumns&&workOrderTypeDiyColumns.size()>0) {
                for (WorkOrderTypeDiyColumn workOrderTypeDiyColumn : workOrderTypeDiyColumns) {
                    if("1".equals(workOrderTypeDiyColumn.getMsgShow())){
                        String columnCode = workOrderTypeDiyColumn.getColumnCode();
                        String componentType = workOrderTypeDiyColumn.getComponentType();
                        String columnName = workOrderTypeDiyColumn.getColumnName();
                        String autoInputFormColumn = workOrderTypeDiyColumn.getAutoInputFromColumn();
                        String duplicatedMark = workOrderTypeDiyColumn.getDuplicatedMark();
                        String isShow = workOrderTypeDiyColumn.getIsShow();
                        String multiple = workOrderTypeDiyColumn.getMultiple();
                        String dictJson = workOrderTypeDiyColumn.getDictJson();
                        String val_dictText = "";
                        String val = "";
                        if (!StringUtil.isNullOrEmpty(columnCode) && null != relJson.get(columnCode)) {
                            val = (String) relJson.get(columnCode);
                            if (!StringUtil.isNullOrEmpty(val)) {
                                if (WorkConst.COMPONENT_TYPE_WENBEN.equals(componentType) ||
                                        WorkConst.COMPONENT_TYPE_DUOHANGWENBEN.equals(componentType) ||
                                        WorkConst.COMPONENT_TYPE_RIQI.equals(componentType) ||
                                        WorkConst.COMPONENT_TYPE_SHIJIAN.equals(componentType)
                                ) {
                                    val_dictText = val;
                                }
                                if (WorkConst.COMPONENT_TYPE_TUPIAN.equals(componentType) ||
                                        WorkConst.COMPONENT_TYPE_WENJIAN.equals(componentType)) {
                                    String[] arr = val.split(",");
                                    for (String str : arr) {
                                        if (!StringUtil.isNullOrEmpty(str)) {
                                            val_dictText += "/jeecgboot/lst-boot/sys/common/static/" + str;
                                        }

                                    }
                                    if (val_dictText.endsWith(",")) {
                                        val_dictText = val_dictText.substring(0, val_dictText.length() - 1);
                                    }

                                }
                                if (WorkConst.COMPONENT_TYPE_XIALAKUANG.equals(componentType)) {


                                    JSONArray jsonArray = JSONArray.parseArray(dictJson);
                                    if (null != jsonArray && jsonArray.size() > 0) {
                                        String[] strs = val.split(",");
                                        if (null != strs && strs.length > 0) {
                                            for (String str : strs) {
                                                for (int i = 0; i < jsonArray.size(); i++) {
                                                    JSONObject jsonObjectTmp = (JSONObject) jsonArray.get(i);
                                                    if (null != jsonObjectTmp) {

                                                        if (null != jsonObjectTmp.get("value") && null != jsonObjectTmp.get("label")) {
                                                            if (str.equals((String) jsonObjectTmp.get("value"))) {
                                                                String text = (String) jsonObjectTmp.get("label");
                                                                val_dictText += text + ",";
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                    if (val_dictText.endsWith(",")) {
                                        val_dictText = val_dictText.substring(0, val_dictText.length() - 1);
                                    }
                                }
                            }
                        }
                        if(!StringUtil.isNullOrEmpty(val_dictText)){
                            forms.put(columnName+":",val_dictText);
                        }
                    }

                }
            }
            if(null!=jsonObject.get("contactUser")){
                forms.put("联系人:",(null!=jsonObject.get("contactUser")?(String)jsonObject.get("contactUser"):""));
            }
            if(null!=jsonObject.get("contactPhone")){
                forms.put("联系电话:",(null!=jsonObject.get("contactPhone")?(String)jsonObject.get("contactPhone"):""));
            }
            forms.put("问题地址:",(null!=jsonObject.get("town_dictText")?(String)jsonObject.get("town_dictText"):"")+workOrder.getAddress());
            String success="0";
            String accessToken= Sample.getAccessToken(appKey,appSecret);
            Long taskId=null;
            String res=Sample.sendOaMsg(accessToken,agentId,useridsList,url,title,content,forms);
            if(!StringUtil.isNullOrEmpty(res)){
                JSONObject object=JSONObject.parseObject(res);
                Integer errcode=null!=object.get("errcode")?(Integer)object.get("errcode"):666;
                String errmsg=null!=object.get("errmsg")?(String)object.get("errmsg"):"";
                if(0==errcode&&"ok".equals(errmsg)) {
                    success="1";
                    taskId=null!=object.get("task_id")?(Long)object.get("task_id"):null;
                }
            }
            JSONObject msg=new JSONObject();

            msg.put("title",title);
            msg.put("content",content);
            msg.put("form",forms);
            msg.put("url",url);
            DingMsg dingMsg=new DingMsg();
            dingMsg.setAccessToken(accessToken);
            dingMsg.setAgentId(agentId.toString());
            dingMsg.setReceiveMsg(res);
            dingMsg.setWorkOrderStatus(workOrder.getStatus());
            dingMsg.setSendMsg(JSONObject.toJSONString(msg));
            dingMsg.setTaskId(null!=taskId?taskId.toString():"");
            dingMsg.setSendSuccess(success);
            dingMsg.setToUser(useridsList);
            dingMsg.setWorkOrder(workOrder.getId());
            dingMsg.setWorkNumber(workOrder.getNumber());
            dingMsg.setDingMsgType(dingMsgType);
            dingMsg.setWarnTimes(warnTimes);
            if(StringUtil.isNullOrEmpty(useridsList)){
                log.error("用户id为空");
            }else{
                dingMsgService.save(dingMsg);
            }

        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
    }
}