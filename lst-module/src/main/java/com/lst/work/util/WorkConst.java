package com.lst.work.util;

public class WorkConst {
    public static String ORDER_STATUS_ADD="1";//报障
    public static String ORDER_STATUS_ALIGN="2";//派单
    public static String ORDER_STATUS_GET="3";//接单
    public static String ORDER_STATUS_HANDLE="4";//处理
    public static String ORDER_STATUS_SCORE="5";//评分
    public static String ORDER_STATUS_DONE="6";//完成

    public static String PAIDAN_STATUS_PAIDAN="1";//派单
    public static String PAIDAN_STATUS_ZHUANDAN="2";//转单
    public static String PAIDAN_STATUS_TUIDAN="3";//退单
    public static String PAIDAN_STATUS_CHULI="4";//处理
    public static String PAIDAN_STATUS_CHEHUI="9";//撤回到派单
    public static String PAIDAN_STATUS_PINGJIA="6";//评价
    public static String PAIDAN_STATUS_TUIHUI="5";//评价退单
    public static String PAIDAN_STATUS_GET="7";//接单
    public static String PAIDAN_STATUS_PAIDANSUBAN="8";//派单人快办

    public static String DIY_SCOPE_BAOZHANG="1";//报障
    public static String DIY_SCOPE_CHULI="2";//处理
    public static String DIY_SCOPE_PINGJIA="3";//评价

    public static String COMPONENT_TYPE_WENBEN="1";//文本
    public static String COMPONENT_TYPE_DUOHANGWENBEN="2";//多行文本
    public static String COMPONENT_TYPE_RIQI="3";//日期
    public static String COMPONENT_TYPE_SHIJIAN="4";//时间
    public static String COMPONENT_TYPE_TUPIAN="5";//图片
    public static String COMPONENT_TYPE_WENJIAN="6";//文件
    public static String COMPONENT_TYPE_XIALAKUANG="0";//下拉框

    public static String DING_MSG_TYPE_COMMON="1";//普通消息
    public static String DING_MSG_TYPE_DEADLINE="2";//超时提醒
    public static String DING_MSG_TYPE_DEADLINE_TOALIGN="3";//超时提醒抄送派单

    public static String MSG_TYPE_WANCHENG="wancheng";//完成
    public static String MSG_TYPE_CHULI="chuli";//处理
    public static String MSG_TYPE_QUEREN="queren";//确认
    public static String MSG_TYPE_GET="get";//接单
    public static String MSG_TYPE_PAIDAN="paidan";//派单
    public static String MSG_TYPE_DEADLINE="deadline";//到期
    public static String MSG_TYPE_DEADLINE_CHAOSONGPAIDAN="deadline_chaosongpaidan";//抄送派单

    public static String SYS_SETTING_ROOTREGION="rootRegion";//默认根地区编码

    public static String sms_template_shouli="SMS_276386663";
    public static String sms_template_jiedan="SMS_276381659";
    public static String sms_template_wancheng="SMS_276371706";
    public static String baozhangType_neibu="1";
    public static String baozhangType_kehu="2";
}
