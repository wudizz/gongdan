package com.lst.work.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 超时提醒
 * @Author: jeecg-boot
 * @Date:   2022-09-03
 * @Version: V1.0
 */
@Data
@TableName("deadline_msg")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="deadline_msg对象", description="超时提醒")
public class DeadlineMsg implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**开始提醒距超时时间小时数*/
	@Excel(name = "开始提醒距超时时间小时数", width = 15)
    @ApiModelProperty(value = "开始提醒距超时时间小时数")
    @TableField(strategy = FieldStrategy.IGNORED)
    private BigDecimal beginHour;

	/**重复提醒间隔小时数*/
	@Excel(name = "重复提醒间隔小时数", width = 15)
    @ApiModelProperty(value = "重复提醒间隔小时数")
    @TableField(strategy = FieldStrategy.IGNORED)
    private BigDecimal intervalHour;
	/**服务项目*/
	@Excel(name = "服务项目", width = 15, dictTable = "work_order_type", dicText = "name", dicCode = "id")
	@Dict(dictTable = "work_order_type", dicText = "name", dicCode = "id")
    @ApiModelProperty(value = "服务项目")
    private java.lang.String workOrderType;
	/**提醒环节*/
	@Excel(name = "提醒环节", width = 15, dicCode = "work_order_status")
	@Dict(dicCode = "work_order_status")
    @ApiModelProperty(value = "提醒环节")
    private java.lang.String status;
	/**最多提醒次数*/
	@Excel(name = "最多提醒次数", width = 15)
    @ApiModelProperty(value = "最多提醒次数")
    @TableField(strategy = FieldStrategy.IGNORED)
    private java.lang.Integer maxAlarmTimes;
	/**是否生效*/
	@Excel(name = "是否生效", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "是否生效")
    private java.lang.String isValid;
}
