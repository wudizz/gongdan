package com.lst.work.service.impl;

import com.lst.work.entity.WorkOrderRealign;
import com.lst.work.mapper.WorkOrderRealignMapper;
import com.lst.work.service.IWorkOrderRealignService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 工单分配记录
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
@Service
public class WorkOrderRealignServiceImpl extends ServiceImpl<WorkOrderRealignMapper, WorkOrderRealign> implements IWorkOrderRealignService {

}
