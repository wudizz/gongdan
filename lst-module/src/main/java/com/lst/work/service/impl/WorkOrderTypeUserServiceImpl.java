package com.lst.work.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lst.work.entity.WorkOrderTypeUser;
import com.lst.work.mapper.WorkOrderTypeUserMapper;
import com.lst.work.service.IWorkOrderTypeUserService;
import io.netty.util.internal.StringUtil;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 工单类型派单用户
 * @Author: jeecg-boot
 * @Date:   2022-08-12
 * @Version: V1.0
 */
@Service
public class WorkOrderTypeUserServiceImpl extends ServiceImpl<WorkOrderTypeUserMapper, WorkOrderTypeUser> implements IWorkOrderTypeUserService {

    @Override
    public void editPlus(String typeId,String userId){
        if(!StringUtil.isNullOrEmpty(typeId)&&!StringUtil.isNullOrEmpty(userId)){
            String[] userIdArr=userId.split(",");
            if(null!=userIdArr&&userIdArr.length>0){
                QueryWrapper<WorkOrderTypeUser> workOrderTypeUserQueryWrapper=new QueryWrapper<>();
                workOrderTypeUserQueryWrapper.eq("work_order_type",typeId);
                this.remove(workOrderTypeUserQueryWrapper);
                List<WorkOrderTypeUser> workOrderTypeUsers=new ArrayList<>();
                for(String user : userIdArr){
                    WorkOrderTypeUser workOrderTypeUser=new WorkOrderTypeUser();
                    //workOrderTypeUser.setTenantId(tenantId);
                    workOrderTypeUser.setWorkOrderType(typeId);
                    workOrderTypeUser.setAlignUser(user);
                    workOrderTypeUsers.add(workOrderTypeUser);
                }
                this.saveBatch(workOrderTypeUsers);
            }
        }
    }
}
