package com.lst.work.service;

import com.lst.work.entity.Town;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 镇
 * @Author: jeecg-boot
 * @Date:   2022-08-30
 * @Version: V1.0
 */
public interface ITownService extends IService<Town> {

}
