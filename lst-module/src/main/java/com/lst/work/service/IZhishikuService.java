package com.lst.work.service;

import com.lst.work.entity.Zhishiku;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 知识库
 * @Author: jeecg-boot
 * @Date:   2022-10-16
 * @Version: V1.0
 */
public interface IZhishikuService extends IService<Zhishiku> {

}
