package com.lst.work.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lst.work.entity.WorkOrderType;
import com.lst.work.entity.WorkOrderTypeUser;
import com.lst.work.entity.WorkOrderTypeView;
import com.lst.work.mapper.WorkOrderTypeMapper;
import com.lst.work.service.IWorkOrderTypeService;
import com.lst.work.service.IWorkOrderTypeUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 工单类型
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
@Service
public class WorkOrderTypeServiceImpl extends ServiceImpl<WorkOrderTypeMapper, WorkOrderType> implements IWorkOrderTypeService {

    @Autowired
    private IWorkOrderTypeUserService workOrderTypeUserService;

    @Override
    public boolean editPlus(WorkOrderTypeView workOrderTypeView) {
        try {
            WorkOrderType workOrderType = new WorkOrderType();
            BeanUtils.copyProperties(workOrderTypeView, workOrderType);
            this.updateById(workOrderType);
            if (null != workOrderTypeView.getAlignUser() && workOrderTypeView.getAlignUser().size() > 0) {
                QueryWrapper<WorkOrderTypeUser> workOrderTypeUserQueryWrapper = new QueryWrapper<WorkOrderTypeUser>();
                workOrderTypeUserQueryWrapper.eq("work_order_type", workOrderTypeView.getId());
                workOrderTypeUserService.remove(workOrderTypeUserQueryWrapper);

                for (String str : workOrderTypeView.getAlignUser()) {
                    WorkOrderTypeUser workOrderTypeUser = new WorkOrderTypeUser();
                    workOrderTypeUser.setWorkOrderType(workOrderTypeView.getId());
                    workOrderTypeUser.setAlignUser(str);
                    workOrderTypeUserService.save(workOrderTypeUser);
                }
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
            return false;
        }
        return true;
    }

    @Override
    public boolean addPlus(WorkOrderTypeView workOrderTypeView) {
        try {
            WorkOrderType workOrderType = new WorkOrderType();
            BeanUtils.copyProperties(workOrderTypeView, workOrderType);
            this.save(workOrderType);
            if (null != workOrderTypeView.getAlignUser() && workOrderTypeView.getAlignUser().size() > 0) {
                for (String str : workOrderTypeView.getAlignUser()) {
                    WorkOrderTypeUser workOrderTypeUser = new WorkOrderTypeUser();
                    workOrderTypeUser.setWorkOrderType(workOrderType.getId());
                    workOrderTypeUser.setAlignUser(str);
                    workOrderTypeUserService.save(workOrderTypeUser);
                    workOrderTypeView.setId(workOrderTypeUser.getId());
                }
            }
        }catch (Exception e){
            log.error(e.getMessage(),e);
            return false;
        }
        return true;
    }

}
