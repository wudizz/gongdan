package com.lst.work.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lst.work.controller.WorkOrderController;
import com.lst.work.entity.WorkOrder;
import com.lst.work.entity.WorkOrderType;
import com.lst.work.entity.WorkOrderTypeDiyColumn;
import com.lst.work.job.SendMsgTask;
import com.lst.work.mapper.WorkOrderMapper;
import com.lst.work.service.IWorkOrderService;
import com.lst.work.service.IWorkOrderTypeDiyColumnService;
import com.lst.work.service.IWorkOrderTypeService;
import com.lst.work.util.WorkConst;
import io.netty.util.internal.StringUtil;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: 工单
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
@Service
public class WorkOrderServiceImpl extends ServiceImpl<WorkOrderMapper, WorkOrder> implements IWorkOrderService {

    @Autowired
    ISysBaseAPI sysBaseAPI;
    @Autowired
    IWorkOrderTypeDiyColumnService workOrderTypeDiyColumnService;
    @Autowired
    IWorkOrderTypeService workOrderTypeService;
    public static HSSFCellStyle getTitleStyle(HSSFWorkbook workbook) {
        // 产生Excel表头
        HSSFCellStyle titleStyle = workbook.createCellStyle();
        titleStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
        titleStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        return titleStyle;
    }
    public static HSSFCellStyle getHeadStyle(HSSFWorkbook workbook) {
        // 产生Excel表头
        HSSFCellStyle titleStyle = workbook.createCellStyle();
        titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        titleStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        return titleStyle;
    }
    public static HSSFCellStyle getNumberStyle(HSSFWorkbook workbook) {
        // 产生Excel表头
        HSSFCellStyle titleStyle = workbook.createCellStyle();
        titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        titleStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        titleStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0"));
        return titleStyle;
    }
    public static HSSFCellStyle getPercentrStyle(HSSFWorkbook workbook) {
        // 产生Excel表头
        HSSFCellStyle titleStyle = workbook.createCellStyle();
        titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        titleStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
        titleStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0%"));
        return titleStyle;
    }
    public Sheet loadSheetPlans(HSSFWorkbook workbook, String title, List<WorkOrder> ents,String workOrderType    ){
        Sheet sheet = workbook.createSheet(title);
        // 设置默认的宽和高
//        sheet.setDefaultRowHeight(200 * 256);
        sheet.setDefaultRowHeight((short) (30 * 20));

        QueryWrapper<WorkOrderTypeDiyColumn> workOrderTypeDiyColumnQueryWrapper=new QueryWrapper<WorkOrderTypeDiyColumn>();
        workOrderTypeDiyColumnQueryWrapper.eq("is_show","1");
        workOrderTypeDiyColumnQueryWrapper.orderByAsc("order_num");
        List<WorkOrderTypeDiyColumn> workOrderTypeDiyColumns=workOrderTypeDiyColumnService.list(workOrderTypeDiyColumnQueryWrapper);
        List<WorkOrderTypeDiyColumn> baozhangScope=new ArrayList<WorkOrderTypeDiyColumn> ();
        List<WorkOrderTypeDiyColumn> chuliScope=new ArrayList<WorkOrderTypeDiyColumn> ();
        List<WorkOrderTypeDiyColumn> pingjiaScope=new ArrayList<WorkOrderTypeDiyColumn> ();
        if(null!=workOrderTypeDiyColumns&&workOrderTypeDiyColumns.size()>0) {
            baozhangScope=workOrderTypeDiyColumns.stream().filter(item->!StringUtil.isNullOrEmpty(workOrderType)&&workOrderType.equals(item.getWorkOrderType())&&WorkConst.DIY_SCOPE_BAOZHANG.equals(item.getScope())).collect(Collectors.toList());
            chuliScope=workOrderTypeDiyColumns.stream().filter(item->!StringUtil.isNullOrEmpty(workOrderType)&&workOrderType.equals(item.getWorkOrderType())&&WorkConst.DIY_SCOPE_CHULI.equals(item.getScope())).collect(Collectors.toList());
            pingjiaScope=workOrderTypeDiyColumns.stream().filter(item->!StringUtil.isNullOrEmpty(workOrderType)&&workOrderType.equals(item.getWorkOrderType())&&WorkConst.DIY_SCOPE_PINGJIA.equals(item.getScope())).collect(Collectors.toList());
        }

        int baozhangScopeInt=null!=baozhangScope&&baozhangScope.size()>0?12+baozhangScope.size():12;
        int paidanScopeInt=5;
        int jiedanScopeInt=4;
        int chuliScopeInt=null!=chuliScope&&chuliScope.size()>0?4+chuliScope.size():4;
        int pinggjiaScopeInt=null!=pingjiaScope&&pingjiaScope.size()>0?3+pingjiaScope.size():3;
        CellRangeAddress addTitle=new CellRangeAddress(0,0,0,baozhangScopeInt+paidanScopeInt+jiedanScopeInt+chuliScopeInt+pinggjiaScopeInt-1);
        CellRangeAddress baozhangScopeAddress=new CellRangeAddress(1,1,0,baozhangScopeInt-1);
        CellRangeAddress paidanScopeAddress=new CellRangeAddress(1,1,baozhangScopeInt,baozhangScopeInt+paidanScopeInt-1);
        CellRangeAddress jiedanScopeAddress=new CellRangeAddress(1,1,baozhangScopeInt+paidanScopeInt,baozhangScopeInt+paidanScopeInt+jiedanScopeInt-1);
        CellRangeAddress chuliScopeAddress=new CellRangeAddress(1,1,baozhangScopeInt+paidanScopeInt+jiedanScopeInt,baozhangScopeInt+paidanScopeInt+jiedanScopeInt+chuliScopeInt-1);
        CellRangeAddress pingjiaScopeAddress=new CellRangeAddress(1,1,baozhangScopeInt+paidanScopeInt+jiedanScopeInt+chuliScopeInt,baozhangScopeInt+paidanScopeInt+jiedanScopeInt+chuliScopeInt+pinggjiaScopeInt-1);
        sheet.addMergedRegion(addTitle);
        sheet.addMergedRegion(baozhangScopeAddress);
        sheet.addMergedRegion(paidanScopeAddress);
        sheet.addMergedRegion(jiedanScopeAddress);
        sheet.addMergedRegion(chuliScopeAddress);
        sheet.addMergedRegion(pingjiaScopeAddress);

        List<JSONObject> array=sysBaseAPI.parseDictTextList(ents);



        int rindex = 0;
        int cindex = 0;
        CellStyle titleStyle=getTitleStyle(workbook);

        //第一行
        Row row = sheet.createRow(rindex);

        CellStyle headStyle=getHeadStyle(workbook);
        Cell cell = row.createCell(cindex);
        RichTextString text = new HSSFRichTextString("工单列表");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);

        //第二行
        rindex++;
        cindex=0;
        row = sheet.createRow(rindex);
        //服务项目

        cell = row.createCell(cindex);
        text = new HSSFRichTextString("报障");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);

        cindex+=baozhangScopeInt;
        
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("派单");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);

        cindex+=paidanScopeInt;
        
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("接单");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);
        cindex+=jiedanScopeInt;
        
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("处理");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);
        cindex+=chuliScopeInt;
        
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("确认");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle );

        rindex++;
        cindex=0;
        row = sheet.createRow(rindex);
        //服务项目
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("服务项目");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);


        //状态
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("状态");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);
        //工单编号
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("工单编号");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);
        //报障人
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("报障人");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);
        //报障时间
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("报障时间");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);

        //60天重复报障
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("60天重复次数");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);

        //报障域自定义字段
        if(null!=baozhangScope&&baozhangScope.size()>0){
            for(WorkOrderTypeDiyColumn workOrderTypeDiyColumn:baozhangScope){
                String columnCode=workOrderTypeDiyColumn.getColumnCode();
                String columnName=workOrderTypeDiyColumn.getColumnName();
                cindex++;
                sheet.setColumnWidth(cindex,(short) (240 * 20));
                cell = row.createCell(cindex);
                text = new HSSFRichTextString(columnName);
                cell.setCellValue(text);
                cell.setCellStyle(headStyle);
            }
        }
        //联系人
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("联系人");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);
        //联系电话
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("联系电话");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);
        //镇
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("镇");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);
        //地址
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("详细地址");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);
        //问题备注
        cindex++;
        
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("问题备注");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);
        //附件
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("附件");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);



        //派单人
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("派单人");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);

        //派单时间
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("派单时间");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);

        //工程师
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("工程师");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);

//        //派单环节耗时
//        cindex++;
//        
//        cell = row.createCell(cindex);
//        text = new HSSFRichTextString("派单环节耗时");
//        cell.setCellValue(text);
//        cell.setCellStyle(titleStyle);

        //快速处理
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("是否快速处理");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);

        //快速处理
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("快速处理说明");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);


        //接单人
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("接单人");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);

        //接单时间
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("接单时间");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);

        //预约服务时间
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("预约服务时间");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);

        //接单时长
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("接单时长");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);

        //处理人
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("处理人");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);

        //报障域自定义字段
        if(null!=chuliScope&&chuliScope.size()>0){
            for(WorkOrderTypeDiyColumn workOrderTypeDiyColumn:chuliScope){
                String columnCode=workOrderTypeDiyColumn.getColumnCode();
                String columnName=workOrderTypeDiyColumn.getColumnName();
                cindex++;
                sheet.setColumnWidth(cindex,(short) (240 * 20));
                cell = row.createCell(cindex);
                text = new HSSFRichTextString(columnName);
                cell.setCellValue(text);
                cell.setCellStyle(headStyle);
            }
        }

        //定位位置
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("定位位置");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);

        //处理时间
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("处理时间");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);
        //处理时间
        cindex++;

        cell = row.createCell(cindex);
        text = new HSSFRichTextString("处理时长");
        cell.setCellValue(text);
        cell.setCellStyle(titleStyle);

        //确认人
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("确认人");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);

        //报障域自定义字段
        if(null!=pingjiaScope&&pingjiaScope.size()>0){
            for(WorkOrderTypeDiyColumn workOrderTypeDiyColumn:pingjiaScope){
                String columnCode=workOrderTypeDiyColumn.getColumnCode();
                String columnName=workOrderTypeDiyColumn.getColumnName();
                cindex++;
                sheet.setColumnWidth(cindex,(short) (240 * 20));
                cell = row.createCell(cindex);
                text = new HSSFRichTextString(columnName);
                cell.setCellValue(text);
                cell.setCellStyle(headStyle);
            }
        }

        //确认时间
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("确认时间");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);

        //确认环节耗时
        cindex++;
        sheet.setColumnWidth(cindex,(short) (240 * 20));
        cell = row.createCell(cindex);
        text = new HSSFRichTextString("工单耗时");
        cell.setCellValue(text);
        cell.setCellStyle(headStyle);


        HSSFCellStyle numberStyle=getNumberStyle(workbook);
        HSSFCellStyle percentStyle=getPercentrStyle(workbook);
        for(JSONObject item : array){
            WorkOrder workOrder=new WorkOrder();
            rindex++;
            row = sheet.createRow(rindex);
            cindex = 0;
            //服务项目
            
            cell = row.createCell(cindex);
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("type_dictText"))?String.valueOf(item.get("type_dictText")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);



            //状态
            cindex++;
            
            cell = row.createCell(cindex);
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("status_dictText"))?String.valueOf(item.get("status_dictText")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);

            //工单编号
            cindex++;
//            workOrder.getNumber();
            
            cell = row.createCell(cindex);
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("number"))?String.valueOf(item.get("number")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);
            //报障人
            cindex++;
            
            cell = row.createCell(cindex);
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("fromUser_dictText"))?String.valueOf(item.get("fromUser_dictText")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);
            //报障时间
            cindex++;
            
            cell = row.createCell(cindex);
//            workOrder.getCreateTime()
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("createTime"))?String.valueOf(item.get("createTime")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);

            //60天重复报障
            cindex++;
            
            cell = row.createCell(cindex);
//            workOrder.getDuplicated60()
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("duplicated60"))?String.valueOf(item.get("duplicated60")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);

            //报障域自定义字段
            if(null!=baozhangScope&&baozhangScope.size()>0) {
                for (WorkOrderTypeDiyColumn workOrderTypeDiyColumn : baozhangScope) {
                    String scope = workOrderTypeDiyColumn.getScope();
                    String columnCode = workOrderTypeDiyColumn.getColumnCode();
                    String componentType = workOrderTypeDiyColumn.getComponentType();
                    String columnName = workOrderTypeDiyColumn.getColumnName();
                    String autoInputFormColumn = workOrderTypeDiyColumn.getAutoInputFromColumn();
                    String duplicatedMark = workOrderTypeDiyColumn.getDuplicatedMark();
                    String multiple = workOrderTypeDiyColumn.getMultiple();
                    String dictJson = workOrderTypeDiyColumn.getDictJson();

                    if (!StringUtil.isNullOrEmpty(columnCode) && null != item.get(columnCode) && null != workOrderTypeDiyColumn.getWorkOrderType() && workOrderTypeDiyColumn.getWorkOrderType().equals(item.get("type"))) {


                        Object val = item.get(columnCode);
                        String val_dictText = "";
                        if (null != val) {
                            if (WorkConst.COMPONENT_TYPE_WENBEN.equals(componentType) ||
                                    WorkConst.COMPONENT_TYPE_DUOHANGWENBEN.equals(componentType) ||
                                    WorkConst.COMPONENT_TYPE_RIQI.equals(componentType) ||
                                    WorkConst.COMPONENT_TYPE_SHIJIAN.equals(componentType)
                            ) {
                                val_dictText = val.toString();
                            }
                            if (WorkConst.COMPONENT_TYPE_TUPIAN.equals(componentType) ||
                                    WorkConst.COMPONENT_TYPE_WENJIAN.equals(componentType)) {
                                String[] arr = val.toString().split(",");
                                for (String str : arr) {
                                    if (!StringUtil.isNullOrEmpty(str)) {
                                        val_dictText += "/jeecgboot/lst-boot/sys/common/static/" + str;
                                    }

                                }
                                if (val_dictText.endsWith(",")) {
                                    val_dictText = val_dictText.substring(0, val_dictText.length() - 1);
                                }

                            }
                            if (WorkConst.COMPONENT_TYPE_XIALAKUANG.equals(componentType)) {
                                String[] strs = null;
                                if (WorkOrderController.isArray(val)) {
                                    strs = (String[]) val;
                                } else {
                                    strs = val.toString().split(",");
                                }

                                JSONArray jsonArray = JSONArray.parseArray(dictJson);
                                if (null != jsonArray && jsonArray.size() > 0) {
                                    if (null != strs && strs.length > 0) {
                                        for (String str : strs) {
                                            for (int i = 0; i < jsonArray.size(); i++) {
                                                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                                                if (null != jsonObject) {

                                                    if (null != jsonObject.get("value") && null != jsonObject.get("label")) {
                                                        if (str.equals((String) jsonObject.get("value"))) {
                                                            String tmpText = (String) jsonObject.get("label");
                                                            val_dictText += tmpText + ",";
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                                if (val_dictText.endsWith(",")) {
                                    val_dictText = val_dictText.substring(0, val_dictText.length() - 1);
                                }
                            }

                        }
                        cindex++;
                        
                        cell = row.createCell(cindex);
                        text = new HSSFRichTextString(val_dictText);
                        cell.setCellValue(text);
                        cell.setCellStyle(titleStyle);
                    }
                }
            }
            //联系人
            cindex++;
            
            cell = row.createCell(cindex);
//            workOrder.getContactUser()
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("contactUser"))?String.valueOf(item.get("contactUser")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);
            //联系电话
            cindex++;
            
            cell = row.createCell(cindex);
//            workOrder.getContactPhone()
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("contactPhone"))?String.valueOf(item.get("contactPhone")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);
            //镇
            cindex++;
            
            cell = row.createCell(cindex);
//            workOrder.getTown()
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("town_dictText"))?String.valueOf(item.get("town_dictText")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);
            //地址
            cindex++;
            
            cell = row.createCell(cindex);
//            workOrder.getAddress()
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("address"))?String.valueOf(item.get("address")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);
            //问题备注
            cindex++;
            
            cell = row.createCell(cindex);
//            workOrder.getRemark()
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("remark"))?String.valueOf(item.get("remark")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);
            //附件
            cindex++;
            
            cell = row.createCell(cindex);
//            workOrder.getFile()
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("file"))?String.valueOf(item.get("file")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);




            //派单人
            cindex++;
            
            cell = row.createCell(cindex);
//            Object aa=item.get("alignUser_dictText");
//            String bb=String.valueOf(item.get("alignUser_dictText"));
//            System.out.println(null!=aa);
//            System.out.println("".equals(bb));
//            System.out.println("null".equals(bb));
            text = new HSSFRichTextString(null!=item.get("alignUser_dictText")&&!"".equals(String.valueOf(item.get("alignUser_dictText")))&&!"null".equals(String.valueOf(item.get("alignUser_dictText")))?String.valueOf(item.get("alignUser_dictText")):"系统自动派单");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);

            //派单时间
            cindex++;
            
            cell = row.createCell(cindex);
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("alignTime"))?String.valueOf(item.get("alignTime")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);

            //工程师
            cindex++;
            
            cell = row.createCell(cindex);
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("toUser_dictText"))?String.valueOf(item.get("toUser_dictText")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);

            //派单环节耗时
//            cindex++;
//            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            Date beginTime=null;
//            Date endTime=null;
//            String haoshi="";
//            if("null"!=String.valueOf(item.get("alignTime"))){
//                try {
//                    beginTime=sdf.parse((String)item.get("createTime"));
//                    endTime=sdf.parse((String)item.get("alignTime"));
//                    haoshi= SendMsgTask.timeConsuming(beginTime,endTime);
//                } catch (ParseException e) {
//                    log.error(e.getMessage(),e);
//                }
//            }

//            
//            cell = row.createCell(cindex);
//            text = new HSSFRichTextString(haoshi);
//            cell.setCellValue(text);
//            cell.setCellStyle(titleStyle);

            //快速处理标记
            cindex++;
            
            cell = row.createCell(cindex);
//            workOrder.getQuickHandleMark()
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("quickHandleMark_dictText"))?String.valueOf(item.get("quickHandleMark_dictText")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);

            //快速处理说明
            cindex++;
            
            cell = row.createCell(cindex);
//            workOrder.getQuickHandleRemark()
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("quickHandleRemark"))?String.valueOf(item.get("quickHandleRemark")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);

            //接单人
            cindex++;
            
            cell = row.createCell(cindex);
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("toUser_dictText"))?String.valueOf(item.get("toUser_dictText")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);

            //接单时间
            cindex++;
            
            cell = row.createCell(cindex);
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("getTime"))?String.valueOf(item.get("getTime")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);





            //预约服务时间
            cindex++;
            
            cell = row.createCell(cindex);
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("planServiceTime"))?String.valueOf(item.get("planServiceTime")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);

            //接单时长


            cindex++;

            cell = row.createCell(cindex);
            SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date beginTime=null;
            Date endTime=null;
            String haoshi="";
            if("null"!=String.valueOf(item.get("alignTime"))&&"null"!=String.valueOf(item.get("getTime"))){
                try {
                    beginTime=sdf.parse((String)item.get("alignTime"));
                    endTime=sdf.parse((String)item.get("getTime"));
                    haoshi= SendMsgTask.timeConsuming(beginTime,endTime);
                } catch (ParseException e) {
                    log.error(e.getMessage(),e);
                }
            }

            text = new HSSFRichTextString(haoshi);
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);

            //处理人
            cindex++;
            
            cell = row.createCell(cindex);
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("toUser_dictText"))?String.valueOf(item.get("toUser_dictText")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);

            //处理域自定义字段
            if(null!=chuliScope&&chuliScope.size()>0) {
                for (WorkOrderTypeDiyColumn workOrderTypeDiyColumn : chuliScope) {
                    String scope = workOrderTypeDiyColumn.getScope();
                    String columnCode = workOrderTypeDiyColumn.getColumnCode();
                    String componentType = workOrderTypeDiyColumn.getComponentType();
                    String columnName = workOrderTypeDiyColumn.getColumnName();
                    String autoInputFormColumn = workOrderTypeDiyColumn.getAutoInputFromColumn();
                    String duplicatedMark = workOrderTypeDiyColumn.getDuplicatedMark();
                    String multiple = workOrderTypeDiyColumn.getMultiple();
                    String dictJson = workOrderTypeDiyColumn.getDictJson();
                    String val_dictText = "";
                    if (!StringUtil.isNullOrEmpty(columnCode) && null != item.get(columnCode) && null != workOrderTypeDiyColumn.getWorkOrderType() && workOrderTypeDiyColumn.getWorkOrderType().equals(item.get("type"))) {


                        Object val = item.get(columnCode);

                        if (null != val) {
                            if (WorkConst.COMPONENT_TYPE_WENBEN.equals(componentType) ||
                                    WorkConst.COMPONENT_TYPE_DUOHANGWENBEN.equals(componentType) ||
                                    WorkConst.COMPONENT_TYPE_RIQI.equals(componentType) ||
                                    WorkConst.COMPONENT_TYPE_SHIJIAN.equals(componentType)
                            ) {
                                val_dictText = val.toString();
                            }
                            if (WorkConst.COMPONENT_TYPE_TUPIAN.equals(componentType) ||
                                    WorkConst.COMPONENT_TYPE_WENJIAN.equals(componentType)) {
                                String[] arr = val.toString().split(",");
                                for (String str : arr) {
                                    if (!StringUtil.isNullOrEmpty(str)) {
                                        val_dictText += "/jeecgboot/lst-boot/sys/common/static/" + str;
                                    }

                                }
                                if (val_dictText.endsWith(",")) {
                                    val_dictText = val_dictText.substring(0, val_dictText.length() - 1);
                                }

                            }
                            if (WorkConst.COMPONENT_TYPE_XIALAKUANG.equals(componentType)) {
                                String[] strs = null;
                                if (WorkOrderController.isArray(val)) {
                                    strs = (String[]) val;
                                } else {
                                    strs = val.toString().split(",");
                                }

                                JSONArray jsonArray = JSONArray.parseArray(dictJson);
                                if (null != jsonArray && jsonArray.size() > 0) {
                                    if (null != strs && strs.length > 0) {
                                        for (String str : strs) {
                                            for (int i = 0; i < jsonArray.size(); i++) {
                                                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                                                if (null != jsonObject) {

                                                    if (null != jsonObject.get("value") && null != jsonObject.get("label")) {
                                                        if (str.equals((String) jsonObject.get("value"))) {
                                                            String tmpText = (String) jsonObject.get("label");
                                                            val_dictText += tmpText + ",";
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                                if (val_dictText.endsWith(",")) {
                                    val_dictText = val_dictText.substring(0, val_dictText.length() - 1);
                                }
                            }

                        }

                    }
                    cindex++;

                    cell = row.createCell(cindex);
                    text = new HSSFRichTextString(val_dictText);
                    cell.setCellValue(text);
                    cell.setCellStyle(titleStyle);
                }
            }

            //定位位置
            cindex++;
            
            cell = row.createCell(cindex);
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("location"))?String.valueOf(item.get("location")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);

            //处理时间
            cindex++;

            cell = row.createCell(cindex);
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("solveTime"))?String.valueOf(item.get("solveTime")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);

            //处理时长
            cindex++;

            cell = row.createCell(cindex);
            beginTime=null;
            endTime=null;
            haoshi="";
            if("null"!=String.valueOf(item.get("alignTime"))&&"null"!=String.valueOf(item.get("solveTime"))){
                try {
                    if("null"!=String.valueOf(item.get("firstAlignTime"))){
                        beginTime=sdf.parse((String)item.get("firstAlignTime"));
                    }else{
                        beginTime=sdf.parse((String)item.get("alignTime"));
                    }
                    endTime=sdf.parse((String)item.get("solveTime"));
                    haoshi= SendMsgTask.timeConsuming(beginTime,endTime);
                } catch (ParseException e) {
                    log.error(e.getMessage(),e);
                }
            }
            text = new HSSFRichTextString(haoshi);
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);



            //确认人
            cindex++;
            
            cell = row.createCell(cindex);

//            text = new HSSFRichTextString("null"!=String.valueOf(item.get("scoreUser_dictText"))?String.valueOf(item.get("scoreUser_dictText")):"");
            text = new HSSFRichTextString(null!=item.get("scoreUser_dictText")&&!"".equals(String.valueOf(item.get("scoreUser_dictText")))&&!"null".equals(String.valueOf(item.get("scoreUser_dictText")))?String.valueOf(item.get("scoreUser_dictText")):"系统自动确认");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);

            //评价域自定义字段
            if(null!=pingjiaScope&&pingjiaScope.size()>0) {
                for (WorkOrderTypeDiyColumn workOrderTypeDiyColumn : pingjiaScope) {
                    String scope = workOrderTypeDiyColumn.getScope();
                    String columnCode = workOrderTypeDiyColumn.getColumnCode();
                    String componentType = workOrderTypeDiyColumn.getComponentType();
                    String columnName = workOrderTypeDiyColumn.getColumnName();
                    String autoInputFormColumn = workOrderTypeDiyColumn.getAutoInputFromColumn();
                    String duplicatedMark = workOrderTypeDiyColumn.getDuplicatedMark();
                    String multiple = workOrderTypeDiyColumn.getMultiple();
                    String dictJson = workOrderTypeDiyColumn.getDictJson();

                    if (!StringUtil.isNullOrEmpty(columnCode)  && null != workOrderTypeDiyColumn.getWorkOrderType() && workOrderTypeDiyColumn.getWorkOrderType().equals(item.get("type"))) {


                        Object val = item.get(columnCode);
                        String val_dictText = "";
                        if (null != val) {
                            if (WorkConst.COMPONENT_TYPE_WENBEN.equals(componentType) ||
                                    WorkConst.COMPONENT_TYPE_DUOHANGWENBEN.equals(componentType) ||
                                    WorkConst.COMPONENT_TYPE_RIQI.equals(componentType) ||
                                    WorkConst.COMPONENT_TYPE_SHIJIAN.equals(componentType)
                            ) {
                                val_dictText = val.toString();
                            }
                            if (WorkConst.COMPONENT_TYPE_TUPIAN.equals(componentType) ||
                                    WorkConst.COMPONENT_TYPE_WENJIAN.equals(componentType)) {
                                String[] arr = val.toString().split(",");
                                for (String str : arr) {
                                    if (!StringUtil.isNullOrEmpty(str)) {
                                        val_dictText += "/jeecgboot/lst-boot/sys/common/static/" + str;
                                    }

                                }
                                if (val_dictText.endsWith(",")) {
                                    val_dictText = val_dictText.substring(0, val_dictText.length() - 1);
                                }

                            }
                            if (WorkConst.COMPONENT_TYPE_XIALAKUANG.equals(componentType)) {
                                String[] strs = null;
                                if (WorkOrderController.isArray(val)) {
                                    strs = (String[]) val;
                                } else {
                                    strs = val.toString().split(",");
                                }

                                JSONArray jsonArray = JSONArray.parseArray(dictJson);
                                if (null != jsonArray && jsonArray.size() > 0) {
                                    if (null != strs && strs.length > 0) {
                                        for (String str : strs) {
                                            for (int i = 0; i < jsonArray.size(); i++) {
                                                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                                                if (null != jsonObject) {

                                                    if (null != jsonObject.get("value") && null != jsonObject.get("label")) {
                                                        if (str.equals((String) jsonObject.get("value"))) {
                                                            String tmpText = (String) jsonObject.get("label");
                                                            val_dictText += tmpText + ",";
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                                if (val_dictText.endsWith(",")) {
                                    val_dictText = val_dictText.substring(0, val_dictText.length() - 1);
                                }
                            }

                        }
                        cindex++;
                        
                        cell = row.createCell(cindex);
                        text = new HSSFRichTextString(val_dictText);
                        cell.setCellValue(text);
                        cell.setCellStyle(titleStyle);
                    }
                }
            }

            //确认时间
            cindex++;
            
            cell = row.createCell(cindex);
            text = new HSSFRichTextString("null"!=String.valueOf(item.get("scoreTime"))?String.valueOf(item.get("scoreTime")):"");
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);

            //工单耗时
            cindex++;
            cell = row.createCell(cindex);
             beginTime=null;
             endTime=null;
             haoshi="";
            if("null"!=String.valueOf(item.get("createTime"))&&"null"!=String.valueOf(item.get("scoreTime"))){
                try {
                    beginTime=sdf.parse((String)item.get("createTime"));
                    endTime=sdf.parse((String)item.get("scoreTime"));
                    haoshi= SendMsgTask.timeConsuming(beginTime,endTime);
                } catch (ParseException e) {
                    log.error(e.getMessage(),e);
                }
            }
            text = new HSSFRichTextString(haoshi);
            cell.setCellValue(text);
            cell.setCellStyle(titleStyle);

        }
        return sheet;
    }
    @Override
    public HSSFWorkbook exportExcel(List<WorkOrder> workOrders){


        List<WorkOrderType> workOrderTypes=workOrderTypeService.list();
        // 声明一个工作薄
        HSSFWorkbook workbook = null;
        workbook= new HSSFWorkbook();
        try {
            List<WorkOrder> orders=new ArrayList<WorkOrder> ();
            if(null!=workOrderTypes&&workOrderTypes.size()>0){
                for(WorkOrderType workOrderType:workOrderTypes){
                    if(null!=workOrders&&workOrders.size()>0) {
                        orders=workOrders.stream().filter(item->workOrderType.getId().equals(item.getType())).collect(Collectors.toList());
                    }
                    loadSheetPlans(workbook ,workOrderType.getName(),orders,workOrderType.getId());
                }
            }

        } catch (Exception e) {
//            e.printStackTrace();
            log.error(e.getMessage(),e);
        }
        return workbook;
    }
}
