package com.lst.work.service;

import com.lst.work.entity.WorkOrderRealign;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 工单分配记录
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
public interface IWorkOrderRealignService extends IService<WorkOrderRealign> {

}
