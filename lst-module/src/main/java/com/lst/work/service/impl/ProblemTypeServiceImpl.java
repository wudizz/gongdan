package com.lst.work.service.impl;

import com.lst.work.entity.ProblemType;
import com.lst.work.mapper.ProblemTypeMapper;
import com.lst.work.service.IProblemTypeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 问题类型
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
@Service
public class ProblemTypeServiceImpl extends ServiceImpl<ProblemTypeMapper, ProblemType> implements IProblemTypeService {

}
