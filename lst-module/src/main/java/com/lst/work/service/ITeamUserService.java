package com.lst.work.service;

import com.lst.work.entity.TeamUser;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 团队用户关系
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
public interface ITeamUserService extends IService<TeamUser> {

    public String synchUserList(String accessToken,String agentId) ;
    public void editPlus(String teamId,String userId);
}
