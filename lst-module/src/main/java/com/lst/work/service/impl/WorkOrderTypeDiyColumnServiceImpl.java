package com.lst.work.service.impl;

import com.lst.work.entity.WorkOrderTypeDiyColumn;
import com.lst.work.mapper.WorkOrderTypeDiyColumnMapper;
import com.lst.work.service.IWorkOrderTypeDiyColumnService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 工单自定义字段
 * @Author: jeecg-boot
 * @Date:   2022-08-26
 * @Version: V1.0
 */
@Service
public class WorkOrderTypeDiyColumnServiceImpl extends ServiceImpl<WorkOrderTypeDiyColumnMapper, WorkOrderTypeDiyColumn> implements IWorkOrderTypeDiyColumnService {

}
