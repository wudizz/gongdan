package com.lst.work.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.gson.JsonObject;
import com.lst.dingtalk.Sample;
import com.lst.work.entity.TeamUser;
import com.lst.work.mapper.TeamUserMapper;
import com.lst.work.service.ITeamUserService;
import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @Description: 团队用户关系
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
@Service
@Slf4j
public class TeamUserServiceImpl extends ServiceImpl<TeamUserMapper, TeamUser> implements ITeamUserService {
    @Autowired
    ISysBaseAPI iSysBaseAPI;
    public String synchUserList(String accessToken,String agentId){
        int dingdingNum=0;
        int addNum=0;
        int updateNum=0;
        int softDelNum=0;
        JSONArray jsonArray=iSysBaseAPI.getAllUser();
        String relStr="";
        String orgName="";
        try {
//            String accessToken = Sample.getAccessToken();
            try{
            Object obj=Sample.getDeptDetail(1,accessToken);
            if(null!=obj){
                JSONObject jsonObject = JSONObject.parseObject(obj.toString());
                Integer errcode = null != jsonObject.get("errcode") ? (Integer) jsonObject.get("errcode") : 666;
                String errmsg = null != jsonObject.get("errmsg") ? (String) jsonObject.get("errmsg") : "";
                if (0 == errcode && "ok".equals(errmsg)) {
                    JSONObject objJson = null != jsonObject.get("result") ? (JSONObject) jsonObject.get("result") : null;
                    orgName = null != objJson.get("name") ? (String) objJson.get("name") : "";
                }

            }
            }catch (Exception e){
                log.error(e.getMessage(),e);
            }
            if(!StringUtil.isNullOrEmpty(orgName)){
                iSysBaseAPI.updateOrgName(orgName);
            }
            System.out.println(accessToken);
            List<Integer> depts = new ArrayList<Integer>();
            depts.add(1);
            Sample.getSonDepart(depts, 1, accessToken, true);
            HashMap<String,JSONObject> userList=new HashMap<String,JSONObject>();
            for (Integer d : depts) {
                List<JSONObject> jsonObjects = Sample.getUserByDept(d, accessToken);
                if(null!=jsonObjects && jsonObjects.size()>0){
                    for(JSONObject jsonObject:jsonObjects){
                        String userid=(String)jsonObject.get(("userid"));
                        if(null==userList.get(userid)){
                            userList.put(userid,jsonObject);
                        }
                    }
                }
            }
            List<String> exists=new ArrayList<String>();
            if(null!=userList&&userList.size()>0){
                dingdingNum=userList.size();
                for (Object key : userList.keySet()) {
                    try{
                        JSONObject jsonObject = (JSONObject)userList.get(key);
                        String name=(null != jsonObject.get("name") ? (String) jsonObject.get("name") : "");
                        String userid=(null != jsonObject.get("userid") ? (String) jsonObject.get("userid") : "");
                        String avatar=(null != jsonObject.get("avatar") ? (String) jsonObject.get("avatar") : "");
                        String phone =(null != jsonObject.get("mobile") ? (String) jsonObject.get("mobile") : "");
                        log.info(JSONObject.toJSONString(jsonObject));
                        JSONObject curUser=null;
                        if(null!=jsonArray&&jsonArray.size()>0){
                            for(int i=0;i<jsonArray.size();i++){
                                JSONObject user=(JSONObject)jsonArray.get(i);
                                if(jsonObject.get("userid").equals(user.get("username"))){
                                    exists.add((String)user.get("username"));
                                    curUser=user;
                                    break;
                                }
                            }
                        }
                        if(null!=curUser){
                            //更新
                            iSysBaseAPI.updateUser(userid,avatar,name,phone);
                            updateNum++;

                        }else{
                            //新增
                            iSysBaseAPI.addUser(userid,avatar,name,phone);
                            addNum++;
                        }
                    }catch (Exception e){
                        log.error(e.getMessage(),e);
                    }

                }
            }
            //删除
            if(null!=jsonArray&&jsonArray.size()>0){
                for(int i=0;i<jsonArray.size();i++){
                    try{
                        JSONObject user=(JSONObject)jsonArray.get(i);
                        List<Object> tmp=exists.stream().filter(item->item.equals(user.get("username"))).collect(Collectors.toList());
                        if((null==tmp || tmp.size()==0)){
                            if(!"admin".equals((String)user.get("username"))){
                                iSysBaseAPI.softDelUser((String)user.get("id"));
                                softDelNum++;
                            }

                        }
                    }catch (Exception e){
                        log.error(e.getMessage(),e);
                    }

                }
            }


        }catch (Exception e){
            log.error(e.getMessage(),e);
        }
        relStr="【钉钉用户——"+dingdingNum+"】【系统新增——"+addNum+"】【系统修改——"+updateNum+"】【系统删除——"+softDelNum+"】";
        log.info(relStr);
        return relStr;
    }

    @Override
    public void editPlus(String teamId,String userId){
        if(!StringUtil.isNullOrEmpty(teamId)&&!StringUtil.isNullOrEmpty(userId)){
            String[] userIdArr=userId.split(",");
            if(null!=userIdArr&&userIdArr.length>0){
                QueryWrapper<TeamUser> workOrderTypeUserQueryWrapper=new QueryWrapper<>();
                workOrderTypeUserQueryWrapper.eq("team",teamId);
                this.remove(workOrderTypeUserQueryWrapper);
                List<TeamUser> workOrderTypeUsers=new ArrayList<>();
                for(String user : userIdArr){
                    TeamUser workOrderTypeUser=new TeamUser();
                    workOrderTypeUser.setTeam(teamId);
                    workOrderTypeUser.setUser(user);
                    workOrderTypeUsers.add(workOrderTypeUser);
                }
                this.saveBatch(workOrderTypeUsers);
            }
        }
    }
}
