package com.lst.work.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import com.lst.work.entity.DeadlineMsg;
import com.lst.work.service.IDeadlineMsgService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 超时提醒
 * @Author: jeecg-boot
 * @Date:   2022-09-03
 * @Version: V1.0
 */
@Api(tags="超时提醒")
@RestController
@RequestMapping("/work/deadlineMsg")
@Slf4j
public class DeadlineMsgController extends JeecgController<DeadlineMsg, IDeadlineMsgService> {
	@Autowired
	private IDeadlineMsgService deadlineMsgService;
	
	/**
	 * 分页列表查询
	 *
	 * @param deadlineMsg
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "超时提醒-分页列表查询")
	@ApiOperation(value="超时提醒-分页列表查询", notes="超时提醒-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(DeadlineMsg deadlineMsg,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<DeadlineMsg> queryWrapper = QueryGenerator.initQueryWrapper(deadlineMsg, req.getParameterMap());
		Page<DeadlineMsg> page = new Page<DeadlineMsg>(pageNo, pageSize);
		IPage<DeadlineMsg> pageList = deadlineMsgService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param deadlineMsg
	 * @return
	 */
	@AutoLog(value = "超时提醒-添加")
	@ApiOperation(value="超时提醒-添加", notes="超时提醒-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody DeadlineMsg deadlineMsg) {
		deadlineMsgService.save(deadlineMsg);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param deadlineMsg
	 * @return
	 */
	@AutoLog(value = "超时提醒-编辑")
	@ApiOperation(value="超时提醒-编辑", notes="超时提醒-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody DeadlineMsg deadlineMsg) {
		deadlineMsgService.updateById(deadlineMsg);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "超时提醒-通过id删除")
	@ApiOperation(value="超时提醒-通过id删除", notes="超时提醒-通过id删除")
	@GetMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		deadlineMsgService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "超时提醒-批量删除")
	@ApiOperation(value="超时提醒-批量删除", notes="超时提醒-批量删除")
	@GetMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.deadlineMsgService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "超时提醒-通过id查询")
	@ApiOperation(value="超时提醒-通过id查询", notes="超时提醒-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		DeadlineMsg deadlineMsg = deadlineMsgService.getById(id);
		if(deadlineMsg==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(deadlineMsg);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param deadlineMsg
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, DeadlineMsg deadlineMsg) {
        return super.exportXls(request, deadlineMsg, DeadlineMsg.class, "超时提醒");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, DeadlineMsg.class);
    }

}
