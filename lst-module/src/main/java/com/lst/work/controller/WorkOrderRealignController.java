package com.lst.work.controller;

import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lst.work.entity.WorkOrderTypeDiyColumn;
import com.lst.work.entity.WorkOrderTypeDiyColumnView;
import com.lst.work.util.WorkConst;
import io.netty.util.internal.StringUtil;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import com.lst.work.entity.WorkOrderRealign;
import com.lst.work.service.IWorkOrderRealignService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 工单分配记录
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
@Api(tags="工单分配记录")
@RestController
@RequestMapping("/work/workOrderRealign")
@Slf4j
public class WorkOrderRealignController extends JeecgController<WorkOrderRealign, IWorkOrderRealignService> {
	@Autowired
	private IWorkOrderRealignService workOrderRealignService;
	@Autowired
	private ISysBaseAPI sysBaseAPI;
	
	/**
	 * 分页列表查询
	 *
	 * @param workOrderRealign
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "工单分配记录-分页列表查询")
	@ApiOperation(value="工单分配记录-分页列表查询", notes="工单分配记录-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(WorkOrderRealign workOrderRealign,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<WorkOrderRealign> queryWrapper = QueryGenerator.initQueryWrapper(workOrderRealign, req.getParameterMap());
		Page<WorkOrderRealign> page = new Page<WorkOrderRealign>(pageNo, pageSize);
		IPage<WorkOrderRealign> pageList = workOrderRealignService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param workOrderRealign
	 * @return
	 */
	//@AutoLog(value = "工单分配记录-添加")
	@ApiOperation(value="工单分配记录-添加", notes="工单分配记录-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody WorkOrderRealign workOrderRealign) {
		workOrderRealignService.save(workOrderRealign);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param workOrderRealign
	 * @return
	 */
	//@AutoLog(value = "工单分配记录-编辑")
	@ApiOperation(value="工单分配记录-编辑", notes="工单分配记录-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody WorkOrderRealign workOrderRealign) {
		workOrderRealignService.updateById(workOrderRealign);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "工单分配记录-通过id删除")
	@ApiOperation(value="工单分配记录-通过id删除", notes="工单分配记录-通过id删除")
	@GetMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		workOrderRealignService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	//@AutoLog(value = "工单分配记录-批量删除")
	@ApiOperation(value="工单分配记录-批量删除", notes="工单分配记录-批量删除")
	@GetMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.workOrderRealignService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "工单分配记录-通过id查询")
	@ApiOperation(value="工单分配记录-通过id查询", notes="工单分配记录-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		WorkOrderRealign workOrderRealign = workOrderRealignService.getById(id);
		if(workOrderRealign==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(workOrderRealign);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param workOrderRealign
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, WorkOrderRealign workOrderRealign) {
        return super.exportXls(request, workOrderRealign, WorkOrderRealign.class, "工单分配记录");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, WorkOrderRealign.class);
    }

}
