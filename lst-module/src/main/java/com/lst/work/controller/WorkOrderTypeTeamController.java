package com.lst.work.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.netty.util.internal.StringUtil;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import com.lst.work.entity.WorkOrderTypeTeam;
import com.lst.work.service.IWorkOrderTypeTeamService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 服务项目指定处理团队
 * @Author: jeecg-boot
 * @Date:   2022-08-26
 * @Version: V1.0
 */
@Api(tags="服务项目指定处理团队")
@RestController
@RequestMapping("/work/workOrderTypeTeam")
@Slf4j
public class WorkOrderTypeTeamController extends JeecgController<WorkOrderTypeTeam, IWorkOrderTypeTeamService> {
	@Autowired
	private IWorkOrderTypeTeamService workOrderTypeTeamService;
	
	/**
	 * 分页列表查询
	 *
	 * @param workOrderTypeTeam
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "服务项目指定处理团队-分页列表查询")
	@ApiOperation(value="服务项目指定处理团队-分页列表查询", notes="服务项目指定处理团队-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(WorkOrderTypeTeam workOrderTypeTeam,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<WorkOrderTypeTeam> queryWrapper = QueryGenerator.initQueryWrapper(workOrderTypeTeam, req.getParameterMap());
		Page<WorkOrderTypeTeam> page = new Page<WorkOrderTypeTeam>(pageNo, pageSize);
		IPage<WorkOrderTypeTeam> pageList = workOrderTypeTeamService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param workOrderTypeTeam
	 * @return
	 */
	@AutoLog(value = "服务项目指定处理团队-添加")
	@ApiOperation(value="服务项目指定处理团队-添加", notes="服务项目指定处理团队-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody WorkOrderTypeTeam workOrderTypeTeam) {
		workOrderTypeTeamService.save(workOrderTypeTeam);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param workOrderTypeTeam
	 * @return
	 */
	@AutoLog(value = "服务项目指定处理团队-编辑")
	@ApiOperation(value="服务项目指定处理团队-编辑", notes="服务项目指定处理团队-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody WorkOrderTypeTeam workOrderTypeTeam) {
		workOrderTypeTeamService.updateById(workOrderTypeTeam);
		return Result.ok("编辑成功!");
	}
	 /**
	  *  编辑
	  *
	  * @return
	  */
	 @ApiOperation(value="工单类型派单用户-编辑", notes="工单类型派单用户-编辑")
	 @GetMapping(value = "/editPlus")
	 public Result<?> editPlus(HttpServletRequest req) {
		 String typeId=req.getParameter("typeId");
		 String teamIds=req.getParameter("teamIds");
		 //String tenantId=req.getParameter("tenantId");

		 if(!StringUtil.isNullOrEmpty(typeId)&&!StringUtil.isNullOrEmpty(teamIds)){
			 workOrderTypeTeamService.editPlus(typeId,teamIds);
		 }else{
			 return Result.ok("数据缺失，请联系管理员!");
		 }
		 return Result.ok("操作成功!");
	 }
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "服务项目指定处理团队-通过id删除")
	@ApiOperation(value="服务项目指定处理团队-通过id删除", notes="服务项目指定处理团队-通过id删除")
	@GetMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		workOrderTypeTeamService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "服务项目指定处理团队-批量删除")
	@ApiOperation(value="服务项目指定处理团队-批量删除", notes="服务项目指定处理团队-批量删除")
	@GetMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.workOrderTypeTeamService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "服务项目指定处理团队-通过id查询")
	@ApiOperation(value="服务项目指定处理团队-通过id查询", notes="服务项目指定处理团队-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		WorkOrderTypeTeam workOrderTypeTeam = workOrderTypeTeamService.getById(id);
		if(workOrderTypeTeam==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(workOrderTypeTeam);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param workOrderTypeTeam
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, WorkOrderTypeTeam workOrderTypeTeam) {
        return super.exportXls(request, workOrderTypeTeam, WorkOrderTypeTeam.class, "服务项目指定处理团队");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, WorkOrderTypeTeam.class);
    }

}
