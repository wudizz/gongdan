import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  base: './',
  server: {
    host: '0.0.0.0',
    proxy: {
      '/ddapi': {
        target: 'https://oapi.dingtalk.com/',
        changeOrigin: true,
        rewrite: path => path.replace(/^\/ddapi/, '')
      },

      '/jeecgboot': {
        target: 'http://localhost:8102/',
        changeOrigin: true,
        rewrite: path => path.replace(/^\/jeecgboot/, '')
      }
    }
  }
})
