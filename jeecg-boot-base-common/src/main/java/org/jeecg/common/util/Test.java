package org.jeecg.common.util;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author ：dage
 * @description ：
 * @date ： 2019/7/29 16:48
 */
public class Test {
    public static void main(String[] args){
        try {
            File file = new File("E:\\count_ent_mon.sql");
            RandomAccessFile fileR = new RandomAccessFile(file,"r");
            // 按行读取字符串
            String str = null;
            int i=0;
            int j=0;
            while ((str = fileR.readLine())!= null) {
                str=new String(str.getBytes("8859_1"), "utf-8");
//                String regex = "GENERATED ALWAYS AS(.*?)VIRTUAL";
//                String regex="INSERT INTO `count_ent_mon`(.*?);";
//                Pattern p_script = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
//                Matcher m_script = p_script.matcher(str);

                String regexFqsg="INSERT INTO `zxjc_rel_fq_sg(.*?)";
                Pattern p_scriptFqsg = Pattern.compile(regexFqsg, Pattern.CASE_INSENSITIVE);
                Matcher m_scriptFqsg = p_scriptFqsg.matcher(str);

                String regexFssg="INSERT INTO `zxjc_rel_fs_sg(.*?)";
                Pattern p_scriptFssg = Pattern.compile(regexFssg, Pattern.CASE_INSENSITIVE);
                Matcher m_scriptFssg = p_scriptFssg.matcher(str);

                String regexWzzsg="INSERT INTO `zxjc_rel_wzz_sg(.*?)";
                Pattern p_scriptWzzsg = Pattern.compile(regexWzzsg, Pattern.CASE_INSENSITIVE);
                Matcher m_scriptWzzsg = p_scriptWzzsg.matcher(str);

                String regexZbsg="INSERT INTO `zxjc_rel_zb_sg(.*?)";
                Pattern p_scriptZbsg = Pattern.compile(regexZbsg, Pattern.CASE_INSENSITIVE);
                Matcher m_scriptZbsg = p_scriptZbsg.matcher(str);

                String regexZssg="INSERT INTO `zxjc_rel_zs_sg(.*?)";
                Pattern p_scriptZssg = Pattern.compile(regexZssg, Pattern.CASE_INSENSITIVE);
                Matcher m_scriptZssg = p_scriptZssg.matcher(str);

                String regexFqzx="INSERT INTO `zxjc_rel_fq_zx(.*?)";
                Pattern p_scriptFqzx = Pattern.compile(regexFqzx, Pattern.CASE_INSENSITIVE);
                Matcher m_scriptFqzx = p_scriptFqzx.matcher(str);

                String regexFszx="INSERT INTO `zxjc_rel_fs_zx(.*?)";
                Pattern p_scriptFszx = Pattern.compile(regexFszx, Pattern.CASE_INSENSITIVE);
                Matcher m_scriptFszx = p_scriptFszx.matcher(str);

                String regexRegion="INSERT INTO `zxjc_rel_fq_sg(.*?)";
                Pattern p_scriptRegion = Pattern.compile(regexRegion, Pattern.CASE_INSENSITIVE);
                Matcher m_scriptRegion = p_scriptRegion.matcher(str);
//                String strRel = m_script.replaceAll(" DEFAULT NULL ");
                if(m_scriptFqsg.find()||m_scriptFssg.find()||m_scriptWzzsg.find()||m_scriptZbsg.find()
                        ||m_scriptZssg.find()||m_scriptFqzx.find()||m_scriptFszx.find()||m_scriptRegion.find()) {
                    write(str + "\r\n");
                    System.out.println(++j+"/"+ ++i+"------"+str);
                }else {
                    System.out.println(++i+"pass");
                }
            }
            fileR.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //

    }
    //按行写入
    public static void write(String data) throws IOException {
        //读写的数据源
        File f = new File("E:\\count_ent_mon_new.sql");
        //指定文件不存在就创建同名文件
        if(!f.exists())    f.createNewFile();
        //rw : 设为读写模式
        RandomAccessFile raf = new RandomAccessFile(f,"rw");
//        System.out.println("当前记录指针位置：" + raf.getFilePointer());
        //记录指针与文件内容长度相等
        raf.seek(raf.length());
//        System.out.println("当前文件长度：" + raf.length());
        //以字节形式写入字符串
        raf.write(data.getBytes());
//        System.out.println("当前记录指针位置：" + raf.getFilePointer());
    }




}
